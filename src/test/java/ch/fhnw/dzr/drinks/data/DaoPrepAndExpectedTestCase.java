package ch.fhnw.dzr.drinks.data;

import org.dbunit.DefaultPrepAndExpectedTestCase;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.PrepAndExpectedTestCaseSteps;
import org.dbunit.VerifyTableDefinition;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;

public class DaoPrepAndExpectedTestCase extends DefaultPrepAndExpectedTestCase {
    private static final String DRIVER = "org.hsqldb.jdbcDriver";
    private static final String URL = "jdbc:hsqldb:mem:drinks";
    private static final String PATH_TO_DATA_FILES = "/ch/fhnw/dzr/drinks/data/";

    private VerifyTableDefinition[] vtds;

    public Object runTest(String prepDataFile, String expectedDataFile, PrepAndExpectedTestCaseSteps steps)
            throws Exception {

        // define prep file as set
        final String[] prepDataFiles = { PATH_TO_DATA_FILES + prepDataFile };
        // define expected file as set
        final String[] expectedDataFiles = { PATH_TO_DATA_FILES + expectedDataFile };
        // run the test
        return runTest(vtds, prepDataFiles, expectedDataFiles, steps);
    }

    public DaoPrepAndExpectedTestCase(VerifyTableDefinition... vtd) throws ClassNotFoundException {
        super(new FlatXmlDataFileLoader(), new JdbcDatabaseTester(DRIVER, URL, "sa", ""));
        this.vtds = vtd;
    }

    @Override
    protected void setUpDatabaseConfig(final DatabaseConfig config) {
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new HsqldbDataTypeFactory());
    }

    @Override
    protected DatabaseOperation getSetUpOperation() {
        return DatabaseOperation.CLEAN_INSERT;
    }
    
}
