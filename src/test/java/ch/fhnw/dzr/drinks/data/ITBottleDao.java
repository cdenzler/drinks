package ch.fhnw.dzr.drinks.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.dbunit.Assertion;
import org.dbunit.VerifyTableDefinition;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DefaultTable;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import ch.fhnw.dzr.drinks.data.jpa.BottleDaoJpa;
import ch.fhnw.dzr.drinks.data.jpa.CategoryDaoJpa;
import ch.fhnw.dzr.drinks.model.Bottle;
import ch.fhnw.dzr.drinks.model.Category;

/**
 * Integration test that tests BottleDaoJpa.
 */
@Tag("integration")
@SuppressWarnings("unchecked")
public class ITBottleDao extends DaoTest<Bottle> {
    private static final String DATA_LOAD = "BottlesFull.xml";
    private static final String DATA_INSERT1 = "BottlesInsert1.xml";
    private static final String DATA_INSERT2 = "BottlesInsert2.xml";
    private static final String DATA_UPDATE1 = "BottlesUpdate1.xml";
    private static final String DATA_UPDATE2 = "BottlesUpdate2.xml";
    private static final String DATA_UPDATE3 = "BottlesUpdate3.xml";
    private static final String DATA_DELETE = "BottlesDelete.xml";

    private static final String TABLE_NAME = "BOTTLES";
    private static final Column[] COLUMNS = new Column[] { 
        new Column("id", DataType.INTEGER),
        new Column("name", DataType.VARCHAR), 
        new Column("description", DataType.VARCHAR),
        new Column("picture", DataType.BLOB),
        new Column("price", DataType.DOUBLE),
        new Column("abv", DataType.INTEGER),
        new Column("instock", DataType.BOOLEAN)};
    private static final Column[] PRIMARY_KEYS = new Column[] { new Column("id", DataType.INTEGER) };

    private BottleDao dao;
    private DaoPrepAndExpectedTestCase tc;

    @BeforeEach
    void setup() throws Exception {
        dao = new BottleDaoJpa(getEMF().createEntityManager());
        var vtd1 = new VerifyTableDefinition(TABLE_NAME, new String[] { "id" });
        var vtd2 = new VerifyTableDefinition("bots_cats", new String[] { });
        var vtd3 = new VerifyTableDefinition("categories", new String[] { "id" });
        tc = new DaoPrepAndExpectedTestCase(vtd1, vtd2, vtd3);
    }

    @DisplayName("get all bottle")
    @Test
    void testGetAllBottles() throws Exception {
        List<Bottle> bottles = (List<Bottle>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getAllBottles();
        });

        ITable actualTable = convertToTable(bottles);
        ITable expectedTable = getExpectedTable(DATA_LOAD);

        Assertion.assertEquals(actualTable, expectedTable);
        
        var bot = bottles.get(4);
        assertEquals(2, bot.getCategories().size());
        var cat1 = bot.getCategories().get(0);
        var cat2 = bot.getCategories().get(1);
        if (cat1.getName().compareTo(cat2.getName()) < 0) {
            assertEquals("Drink", cat1.getName());
            assertEquals("Shortdrink", cat1.getType());
        } else {
            assertEquals("Zutaten", cat1.getName());
            assertEquals("Destillate aus Obst", cat1.getType());
        }
        
        bot = bottles.get(3);
        assertTrue(bot.getCategories().isEmpty());
        
        bot = bottles.get(5);
        assertEquals(1, bot.getCategories().size());
    }
    
    @DisplayName("get single bottle by name")
    @Test
    void testGetSingleBottleByName() throws Exception {
        Optional<Bottle> bot = (Optional<Bottle>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getBottleByName("Mandelsirup");
        });
        assertEquals("Mandelsirup", bot.get().getName());
        assertEquals(4.8, bot.get().getPrice().get(), 0.001);
        assertEquals(2, bot.get().getCategories().size());
    }
    
    @DisplayName("get bottles by name similarity")
    @Test
    void testGetBottlesByName() throws Exception {
        List<Bottle> bottles = (List<Bottle>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getBottlesByName("Whisky", false, false);
        });
        assertEquals(1, bottles.size());

        bottles = dao.getBottlesByName("Whisk%y", false, false);
        assertEquals(2, bottles.size());

        bottles = dao.getBottlesByName("Whisk*y", false, false);
        assertEquals(0, bottles.size());

        bottles = dao.getBottlesByName("WhIsKy", true, true);
        assertEquals(4, bottles.size());

        bottles = dao.getBottlesByName("WHisky", true, false);
        assertEquals(0, bottles.size());

        bottles = dao.getBottlesByName("", true, false);
        assertEquals(11, bottles.size());
    }

    @DisplayName("Test wildcard matching on bottle names.")
    @Test
    void testGetBottlesWithWildcards() throws Exception {
        List<Bottle> bots = (List<Bottle>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getBottlesByName("Whisk%y", true, true);
        });
        assertEquals(6, bots.size());

        bots = dao.getBottlesByName("W%y", true, false);
        assertEquals(6, bots.size());

        bots = dao.getBottlesByName("%", true, false);
        assertEquals(11, bots.size());

        bots = dao.getBottlesByName("", true, false);
        assertEquals(11, bots.size());
    }

    @DisplayName("Test retrieving bottles by their abv.")
    @Test
    void testGetBottlesByAlcohol() throws Exception {
        List<Bottle> bots = (List<Bottle>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getBottleByABV(100);
        });
        assertEquals(11, bots.size());

        bots = dao.getBottleByABV(43);
        assertEquals(10, bots.size());

        bots = dao.getBottleByABV(42);
        assertEquals(9, bots.size());

        bots = dao.getBottleByABV(1);
        assertEquals(6, bots.size());

        bots = dao.getBottleByABV(0);
        assertEquals(5, bots.size());
    }

    @DisplayName("Test retrieving bottles by their availability in stock.")
    @Test
    void testGetBottlesByAvailability() throws Exception {
        List<Bottle> bots = (List<Bottle>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getBottleByAvailability(true);
        });
        assertEquals(7, bots.size());

        bots = dao.getBottleByAvailability(false);
        assertEquals(4, bots.size());
    }

    @DisplayName("Test inserting new Bottle with no categories.")
    @Test
    public void testInsertBottleNoCategories() throws Exception {
        Bottle bot = new Bottle("Hustensaft", 8);
        bot.setPrice(Optional.of(2.71));
        bot.setInStock(true);
        bot.setDescription(Optional.of("lecker"));

        tc.runTest(DATA_LOAD, DATA_INSERT1, () -> {
            dao.saveOrUpdate(bot);
            return null;
        });
    }

    @DisplayName("Test inserting new Bottle with two categories.")
    @Test
    public void testInsertBottleWithCategories() throws Exception {
        Bottle bot = new Bottle("Hustensaft", 8);
        bot.setPrice(Optional.of(2.71));
        bot.setInStock(true);
        bot.setDescription(Optional.of("lecker"));
        
        var catdao = new CategoryDaoJpa(getEMF().createEntityManager());
        List<Category> cats = catdao.getAllTypesOf("Zutaten");
        
        bot.addCategory(cats.get(2));
        bot.addCategory(cats.get(3));

        tc.runTest(DATA_LOAD, DATA_INSERT2, () -> {
            dao.saveOrUpdate(bot);
            return null;
        });
    }
    
    @DisplayName("update an existing bottle")
    @Test
    void testUpdate() throws Exception {
        Optional<Bottle> bottle = dao.getBottleByName("Zuckersirup");
        var bot = bottle.get();
        bot.setInStock(false);
        bot.setDescription(Optional.of("Süsser"));
        bot.setPrice(Optional.of(3.141));
        
        tc.runTest(DATA_LOAD, DATA_UPDATE1, () -> {
            dao.saveOrUpdate(bot);
            return null;
        });
    }
    
    @DisplayName("update an existing bottle with a new description (former value: null)")
    @Test
    void testUpdateToNonNull() throws Exception {
        Optional<Bottle> bottle = dao.getBottleByName("Whiskey");
        var bot = bottle.get();
        bot.setDescription(Optional.of("Irisch"));
        var catdao = new CategoryDaoJpa(getEMF().createEntityManager());

        List<Category> cats = catdao.getAllTypesOf("Zutaten");
        
        bot.addCategory(cats.get(3));
        bot.addCategory(cats.get(4));

        
        tc.runTest(DATA_LOAD, DATA_UPDATE2, () -> {
            dao.saveOrUpdate(bot);
            return null;
        });
    }
    
    @DisplayName("update an existing bottle: setting existing price to null")
    @Test
    void testUpdateToNull() throws Exception {  
        Optional<Bottle> bottle = dao.getBottleByName("Mandelsirup");
        var bot = bottle.get();
        bot.setDescription(Optional.empty());
        
        List<Category>list = bot.getCategories();
        var cat = list.get(0).getId() == 2007 ? list.get(0) : list.get(1);
        bot.removeCategory(cat);        
              
        tc.runTest(DATA_LOAD, DATA_UPDATE3, () -> {
            dao.saveOrUpdate(bot);
            return null;
        });
    }

    @DisplayName("Test deleting Bottle.")
    @Test
    public void testDeleteBottle() throws Exception {
        Optional<Bottle> bottle = dao.getBottleByName("Canadian Whisky");
        var bot = bottle.get();
        
        tc.runTest(DATA_LOAD, DATA_DELETE, () -> {
            dao.delete(bot);
            return null; 
        });
        
    }


    @Override
    protected void fillTable(DefaultTable t, List<Bottle> list) throws Exception {
        int row = 0;
        for (Bottle b : list) {
            String desc = b.getDescription().isPresent() ? b.getDescription().get() : null;
            byte[] blob = b.getPicture().isPresent() ? b.getPicture().get() : null;
            Double price = b.getPrice().isPresent() ? b.getPrice().get() : null;
            t.addRow();
            t.setValue(row, "id", b.getId());
            t.setValue(row, "name", b.getName());
            t.setValue(row, "description", desc);
            t.setValue(row, "picture", blob);
            t.setValue(row, "price", price);
            t.setValue(row, "abv", b.getABV());
            t.setValue(row, "instock", b.isInStock());
            row++;        
        }
    }

    @Override
    protected Column[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected Column[] getPrimaryKeys() {
        return PRIMARY_KEYS;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }
}
