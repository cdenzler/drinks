package ch.fhnw.dzr.drinks.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import org.dbunit.Assertion;
import org.dbunit.VerifyTableDefinition;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DefaultTable;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import ch.fhnw.dzr.drinks.data.jpa.GlassDaoJpa;
import ch.fhnw.dzr.drinks.model.Glass;

@Tag("integration")
public class ITGlassDao extends DaoTest<Glass> {

    private static final String DATA_LOAD = "GlassesFull.xml";
    private static final String DATA_UPDATE = "GlassesUpdate.xml";
    private static final String DATA_UPDATE2 = "GlassesUpdate2.xml";
    private static final String DATA_UPDATE3 = "GlassesUpdate3.xml";
    private static final String DATA_UPDATE4 = "GlassesUpdate4.xml";
    private static final String DATA_INSERT = "GlassesInsert.xml";
    private static final String DATA_INSERT2 = "GlassesInsert2.xml";
    private static final String DATA_DELETE = "GlassesDelete.xml";

    private static final String TABLE_NAME = "GLASSES";
    private static final Column[] COLUMNS = new Column[] { 
        new Column("id", DataType.INTEGER),
        new Column("name", DataType.VARCHAR), 
        new Column("description", DataType.VARCHAR),
        new Column("picture", DataType.BLOB) };
    private static final Column[] PRIMARY_KEYS = new Column[] { new Column("id", DataType.INTEGER) };

    private GlassDao dao;
    private VerifyTableDefinition vtd;
    private DaoPrepAndExpectedTestCase tc;

    @BeforeEach
    void setup() throws Exception {
        dao = new GlassDaoJpa(getEMF().createEntityManager());
        vtd = new VerifyTableDefinition(TABLE_NAME, new String[] { "id" });
        tc = new DaoPrepAndExpectedTestCase(vtd);
    }

    @SuppressWarnings("unchecked")
    @DisplayName("get all glasses from db without pictures ")
    @Test
    void testGetAllGlasses() throws Exception {
        List<Glass> glasses = (List<Glass>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getAllGlasses(false);
        });

        ITable actualTable = convertToTable(glasses);
        ITable expectedTable = getExpectedTable(DATA_LOAD);

        Assertion.assertEquals(actualTable, expectedTable);
    }

    @SuppressWarnings("unchecked")
    @DisplayName("get glass by name")
    @Test
    void testGetGlassByName() throws Exception {
        Optional<Glass> glass = (Optional<Glass>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getGlassByName("Tumbler");
        });

        Glass g = glass.get();
        assertEquals("Tumbler", g.getName());
        assertEquals("Halbhohes breites Glas.", g.getDescription().get());

        glass = (Optional<Glass>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getGlassByName("non-exisitng Name");
        });
        assertTrue(glass.isEmpty());
    }

    @DisplayName("insert a new glass (with picture) into the database.")
    @Test
    void testInsertGlassWithPicture() throws Exception {
        // prepare new glass
        Glass g = new Glass("Bierglas");
        g.setDescription(Optional.of("ein hohes dünnes Glas zum Biertrinken."));
        InputStream is = this.getClass().getResourceAsStream("jpeg.jpg");
        g.setPicture(Optional.of(is.readAllBytes()));

        // save new glass
        tc.runTest(DATA_LOAD, DATA_INSERT, () -> {
            dao.saveOrUpdateGlass(g);
            return null;
        });
    }

    @DisplayName("insert a new glass (without picture) into the database.")
    @Test
    void testInsertGlassNoPicture() throws Exception {
        // prepare new glass
        Glass g = new Glass("Plasticbecher");
        g.setDescription(Optional.of("ein einfacher Becher der nicht kaputt gehen kann."));

        // save new glass
        tc.runTest(DATA_LOAD, DATA_INSERT2, () -> {
            dao.saveOrUpdateGlass(g);
            return null;
        });
    }

    @DisplayName("update a glass with picture.")
    @Test
    void testUpdateGlass() throws Exception {
        tc.runTest(DATA_UPDATE2, DATA_UPDATE3, () -> {
            Optional<Glass> glass = dao.getGlassByName("Cocktailglas");
            Glass g = glass.get();
            g.setName("Drinkglas");
            g.setDescription(Optional.of("Hallo"));
            InputStream is = this.getClass().getResourceAsStream("jpeg.jpg");
            g.setPicture(Optional.of(is.readAllBytes()));

            dao.saveOrUpdateGlass(g);
            return null;
        });
    }

    @DisplayName("provide a picture for a glass without picture")
    @Test
    void testUpdateAddGlass() throws Exception {
        tc.runTest(DATA_LOAD, DATA_UPDATE, () -> {
            Optional<Glass> glass = dao.getGlassByName("Tumbler");
            Glass g = glass.get();
            g.setDescription(Optional.of("see picture"));
            InputStream is = this.getClass().getResourceAsStream("jpeg.jpg");
            g.setPicture(Optional.of(is.readAllBytes()));

            dao.saveOrUpdateGlass(g);
            return null;
        });
    }

    @DisplayName("remove a picture from a glass with picture")
    @Test
    void testUpdateRemoveGlass() throws Exception {
        tc.runTest(DATA_LOAD, DATA_UPDATE4, () -> {
            Optional<Glass> g = dao.getGlassByName("Cocktailglas");
            Glass glass = g.get();
            glass.setPicture(null);

            dao.saveOrUpdateGlass(glass);
            return null;
        });
    }

    @DisplayName("delete a glass with picture")
    @Test
    void testDeleteGlass() throws Exception {
        tc.runTest(DATA_LOAD, DATA_DELETE, () -> {
            dao.deleteGlass("Cocktailglas");
            return null;
        });
    }

    @Override
    protected void fillTable(DefaultTable t, List<Glass> glasslist) throws Exception {
        int row = 0;
        for (Glass g : glasslist) {
            String desc = g.getDescription().isPresent() ? g.getDescription().get() : null;
            byte[] blob = g.getPicture().isPresent() ? g.getPicture().get() : null;
            t.addRow();
            t.setValue(row, "id", g.getId());
            t.setValue(row, "name", g.getName());
            t.setValue(row, "description", desc);
            t.setValue(row, "picture", blob);
            row++;
        }
    }

    @Override
    protected Column[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected Column[] getPrimaryKeys() {
        return PRIMARY_KEYS;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }
}
