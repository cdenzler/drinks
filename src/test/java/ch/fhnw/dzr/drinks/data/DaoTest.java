package ch.fhnw.dzr.drinks.data;

import java.io.InputStream;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.dbunit.dataset.Column;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.DefaultTable;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

public abstract class DaoTest<T> {
	
    private static EntityManagerFactory emf;

    @BeforeAll
    static void initialize() throws Exception {
    	emf = Persistence.createEntityManagerFactory("Drinks.Test"); 
    }
    
    @AfterAll
    static void closeDB() throws Exception {
    	emf.close();
    }
    
    protected static EntityManagerFactory getEMF() {
    	return emf;
    }
    
    protected ITable getExpectedTable(String file) throws DataSetException {
        InputStream stream = this.getClass().getResourceAsStream(file);
        IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(stream);
        ITable expectedTable = expectedDataSet.getTable(getTableName());
        return expectedTable;
    }
    
    protected DefaultTable convertToTable(List<T> list) throws Exception {
        ITableMetaData meta = new DaoTableMetaData(getColumns(), getPrimaryKeys(), getTableName());
        DefaultTable t = new DefaultTable(meta);
        fillTable(t, list);
        return t;
    }
    
    protected abstract void fillTable(DefaultTable t, List<T> list) throws Exception;
    
    protected abstract Column[] getColumns();
    
    protected abstract Column[] getPrimaryKeys();
    
    protected abstract String getTableName();
    
}
