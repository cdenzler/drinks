package ch.fhnw.dzr.drinks.data;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dbunit.Assertion;
import org.dbunit.VerifyTableDefinition;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DefaultTable;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.fhnw.dzr.drinks.data.jpa.IngredientDaoJpa;
import ch.fhnw.dzr.drinks.model.Bottle;
import ch.fhnw.dzr.drinks.model.Drink;
import ch.fhnw.dzr.drinks.model.Ingredient;

public class ITIngredientDao extends DaoTest<Ingredient> {

    private static final String DATA_LOAD = "IngredientsFull.xml";
    private static final String DATA_FOR_DRINK = "IngredientsForDrink.xml";
    private static final String DATA_DELETE = "IngredientsDelete.xml";
    private static final String DATA_SAVE = "IngredientsSaveAllFor.xml";
    
    private static final String TABLE_NAME = "INGREDIENTS";
    private static final Column[] COLUMNS = new Column[] { 
        new Column("bottle_id", DataType.INTEGER),
        new Column("qty", DataType.DOUBLE), 
        new Column("unit", DataType.VARCHAR),
        new Column("drink_id", DataType.INTEGER)};
    private static final Column[] PRIMARY_KEYS = new Column[] { new Column("bottle_id", DataType.INTEGER),
                                                                new Column("drink_id", DataType.INTEGER)};

    private IngredientDao dao;
    private DaoPrepAndExpectedTestCase tc;

    @BeforeEach
    void setup() throws Exception {
        dao = new IngredientDaoJpa(getEMF().createEntityManager());
        var vtd = new VerifyTableDefinition[] {
            new VerifyTableDefinition("glasses", new String[] { "id" }),
            new VerifyTableDefinition("bartools", new String[] { }),
            new VerifyTableDefinition("categories", new String[] { }),
            new VerifyTableDefinition("drinks", new String[] { }),
            new VerifyTableDefinition("bottles", new String[] { }),
            new VerifyTableDefinition("ingredients", new String[] { })
        };
        tc = new DaoPrepAndExpectedTestCase(vtd);
    }

    @SuppressWarnings("unchecked")
    @DisplayName("get ingredients for a drink")
    @Test
    @Disabled
    void testGetIngredientsFor() throws Exception {
        
        List<Ingredient> cats = (List<Ingredient>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getIngredientsFor(1002L);
        });

        ITable actualTable = convertToTable(cats);
        ITable expectedTable = this.getExpectedTable(DATA_FOR_DRINK);
        
        Assertion.assertEquals(expectedTable, actualTable);
    }
    
    @DisplayName("delete all ingredients of a drink")
    @Test
    @Disabled
    void testDeleteIngredientsFor() throws Exception {
        tc.runTest(DATA_LOAD, DATA_DELETE, () -> {
            dao.deleteAllFor(1002L);
            return null;
        });
    }
    
    @DisplayName("save new list of ingredients for a drink")
    @Test
    @Disabled
    void testSaveAllFor() throws Exception {
        List<Ingredient> list = new ArrayList<>();
        var drink = mock(Drink.class); when(drink.getId()).thenReturn(1001L);
        var bev1 = mock(Bottle.class); when(bev1.getId()).thenReturn(2003L);
        var bev2 = mock(Bottle.class); when(bev2.getId()).thenReturn(2004L);
        var bev3 = mock(Bottle.class); when(bev3.getId()).thenReturn(2005L);
        var ing1 = new Ingredient(drink, bev1, 4, Optional.of("cl"));
        var ing2 = new Ingredient(drink, bev2, 2, Optional.of("cl"));
        var ing3 = new Ingredient(drink, bev3, 2, Optional.of("cl"));
        list.add(ing1);
        list.add(ing2);
        list.add(ing3);
        tc.runTest(DATA_LOAD, DATA_SAVE, () -> {
            dao.saveAllFor(1001L, list);
            return null; 
        });
    }

    @Override
    protected void fillTable(DefaultTable t, List<Ingredient> list) throws Exception {
        int row = 0;
        for (Ingredient i : list) {
            Class<Ingredient> clazz = Ingredient.class;
            Field f = clazz.getDeclaredField("bottle");
            f.setAccessible(true);
            Bottle b = (Bottle) f.get(i);
            
            String unit = i.getUnit().isPresent() ? i.getUnit().get() : null;
            t.addRow();
            t.setValue(row, "bottle_id", b.getId());
            t.setValue(row, "drink_id", i.getDrink().getId());
            t.setValue(row, "qty", i.getQuantity());
            t.setValue(row, "unit", unit);
            row++;
        }
    }

    @Override
    protected Column[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected Column[] getPrimaryKeys() {
        return PRIMARY_KEYS;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }
    
}
