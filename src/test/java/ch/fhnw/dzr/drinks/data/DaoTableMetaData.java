package ch.fhnw.dzr.drinks.data;

import org.dbunit.dataset.Column;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.dataset.NoSuchColumnException;

public class DaoTableMetaData implements ITableMetaData {
    
    private Column[] cols;
    private Column[] pKeys;
    private String tableName;

    public DaoTableMetaData(Column[] columns, Column[] primaryKeys, String tableName) {
        cols = copyArray(columns);
        pKeys = copyArray(primaryKeys);
        this.tableName = tableName;
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    private Column[] copyArray(Column[] array) {
        Column[] result = new Column[array.length];
        System.arraycopy(array, 0, result, 0, array.length);
        return result;
    }
    
    @Override
    public Column[] getColumns() throws DataSetException {
        return copyArray(cols);
    }

    @Override
    public Column[] getPrimaryKeys() throws DataSetException {
        return copyArray(pKeys);
    }

    @Override
    public int getColumnIndex(String columnName) throws DataSetException {
        int index = 0;
        for (Column c : cols) {
            if (c.getColumnName().equals(columnName.toLowerCase())) {
                return index;
            }
            index++;
        }
        throw new NoSuchColumnException(getTableName(), columnName);
    }

}
