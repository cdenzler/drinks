package ch.fhnw.dzr.drinks.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import org.dbunit.Assertion;
import org.dbunit.VerifyTableDefinition;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DefaultTable;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import ch.fhnw.dzr.drinks.data.jpa.BarToolDaoJpa;
import ch.fhnw.dzr.drinks.model.BarTool;

@Tag("integration")
public class ITBarToollDao extends DaoTest<BarTool> {

    private static final String DATA_LOAD = "BartoolsFull.xml";
    private static final String DATA_UPDATE = "BartoolsUpdate.xml";
    private static final String DATA_UPDATE2 = "BartoolsUpdate2.xml";
    private static final String DATA_UPDATE3 = "BartoolsUpdate3.xml";
    private static final String DATA_UPDATE4 = "BartoolsUpdate4.xml";
    private static final String DATA_INSERT = "BartoolsInsert.xml";
    private static final String DATA_INSERT2 = "BartoolsInsert2.xml";
    private static final String DATA_DELETE = "BartoolsDelete.xml";

    private static final String TABLE_NAME = "BARTOOLS";
    private static final Column[] COLUMNS = new Column[] { 
        new Column("id", DataType.INTEGER),
        new Column("name", DataType.VARCHAR), 
        new Column("description", DataType.VARCHAR),
        new Column("picture", DataType.BLOB) };
    private static final Column[] PRIMARY_KEYS = new Column[] { new Column("id", DataType.INTEGER) };

    private BarToolDao dao;
    private VerifyTableDefinition vtd;
    private DaoPrepAndExpectedTestCase tc;

    @BeforeEach
    void setup() throws Exception {
        dao = new BarToolDaoJpa(getEMF().createEntityManager());
        vtd = new VerifyTableDefinition(TABLE_NAME, new String[] { "id" });
        tc = new DaoPrepAndExpectedTestCase(vtd);
    }

    @SuppressWarnings("unchecked")
    @DisplayName("get all bar tools from db without pictures ")
    @Test
    void testGetAllBarTool() throws Exception {
        List<BarTool> barTools = (List<BarTool>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getAllBarTools(false);
        });
        ITable actualTable = convertToTable(barTools);
        ITable expectedTable = getExpectedTable(DATA_LOAD);

        Assertion.assertEquals(actualTable, expectedTable);
    }

    @SuppressWarnings("unchecked")
    @DisplayName("get bar tool by name")
    @Test
    void testGetBarToolByName() throws Exception {
        Optional<BarTool> barTool = (Optional<BarTool>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getBarToolByName("Tumbler");
        });

        BarTool u = barTool.get();
        assertEquals("Tumbler", u.getName());
        assertEquals("Halbhohes breites Glas.", u.getDescription().get());

        barTool = (Optional<BarTool>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getBarToolByName("non-exisitng Name");
        });
        assertTrue(barTool.isEmpty());
    }

    @DisplayName("insert a new bar tool (with picture) into the database.")
    @Test
    void testInsertBarToolWithPicture() throws Exception {
        // prepare new bar tool
        BarTool u = new BarTool("Bierglas");
        u.setDescription(Optional.of("ein hohes dünnes Glas zum Biertrinken."));
        InputStream is = this.getClass().getResourceAsStream("jpeg.jpg");
        u.setPicture(Optional.of(is.readAllBytes()));

        // save new bar tool
        tc.runTest(DATA_LOAD, DATA_INSERT, () -> {
            dao.saveOrUpdateBarTool(u);
            return null;
        });
    }

    @DisplayName("insert a new bar tool (without picture) into the database.")
    @Test
    void testInsertBarToolNoPicture() throws Exception {
        // prepare new bar tool
        BarTool u = new BarTool("Plasticbecher");
        u.setDescription(Optional.of("ein einfacher Becher der nicht kaputt gehen kann."));

        // save new bar tool
        tc.runTest(DATA_LOAD, DATA_INSERT2, () -> {
            dao.saveOrUpdateBarTool(u);
            return null;
        });
    }

    @DisplayName("update a bar tool with picture.")
    @Test
    void testUpdateBarTool() throws Exception {
        tc.runTest(DATA_UPDATE2, DATA_UPDATE3, () -> {
            Optional<BarTool> barTool = dao.getBarToolByName("Cocktailglas");
            BarTool u = barTool.get();
            u.setDescription(Optional.of("blabla"));
            InputStream is = this.getClass().getResourceAsStream("jpeg.jpg");
            u.setPicture(Optional.of(is.readAllBytes()));

            dao.saveOrUpdateBarTool(u);
            return null;
        });
    }

    @DisplayName("provide a picture for a bar tool without picture")
    @Test
    void testUpdateAddBarTool() throws Exception {
        tc.runTest(DATA_LOAD, DATA_UPDATE, () -> {
            Optional<BarTool> barTool = dao.getBarToolByName("Tumbler");
            BarTool u = barTool.get();
            u.setDescription(Optional.of("see picture"));
            InputStream is = this.getClass().getResourceAsStream("jpeg.jpg");
            u.setPicture(Optional.of(is.readAllBytes()));

            dao.saveOrUpdateBarTool(u);
            return null;
        });
    }

    @DisplayName("remove a picture from a bar tool with picture")
    @Test
    void testUpdateRemoveBarTool() throws Exception {
        tc.runTest(DATA_LOAD, DATA_UPDATE4, () -> {
            Optional<BarTool> barTool = dao.getBarToolByName("Cocktailglas");
            BarTool u = barTool.get();
            u.setPicture(null);

            dao.saveOrUpdateBarTool(u);
            return null;
        });
    }

    @DisplayName("delete a bar tool with picture")
    @Test
    void testDeleteBarTool() throws Exception {
        tc.runTest(DATA_LOAD, DATA_DELETE, () -> {
            dao.deleteBarTool("Cocktailglas");
            return null;
        });
    }

    @Override
    protected void fillTable(DefaultTable t, List<BarTool> list) throws Exception {
        int row = 0;
        for (BarTool u : list) {
            String desc = u.getDescription().isPresent() ? u.getDescription().get() : null;
            byte[] blob = u.getPicture().isPresent() ? u.getPicture().get() : null;
            t.addRow();
            t.setValue(row, "id", u.getId());
            t.setValue(row, "name", u.getName());
            t.setValue(row, "description", desc);
            t.setValue(row, "picture", blob);
            row++;
        }
    }

    @Override
    protected Column[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected Column[] getPrimaryKeys() {
        return PRIMARY_KEYS;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }
}
