package ch.fhnw.dzr.drinks.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.dbunit.VerifyTableDefinition;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DefaultTable;
import org.dbunit.dataset.datatype.DataType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import ch.fhnw.dzr.drinks.data.jpa.DrinkDaoJpa;
import ch.fhnw.dzr.drinks.model.BarTool;
import ch.fhnw.dzr.drinks.model.Bottle;
import ch.fhnw.dzr.drinks.model.Category;
import ch.fhnw.dzr.drinks.model.Drink;
import ch.fhnw.dzr.drinks.model.DrinksStorageException;
import ch.fhnw.dzr.drinks.model.Glass;

@Tag("integration")
@SuppressWarnings("unchecked")
class ITDrinkDao extends DaoTest<Drink> {
    
    private static final String DATA_LOAD = "DrinksFull.xml";
    private static final String DATA_INSERT = "DrinksInsert.xml";
    private static final String DATA_UPDATE = "DrinksUpdate.xml";
    private static final String DATA_DELETE = "DrinksDelete.xml";
    
    private static final String TABLE_NAME = "BARTOOLS";
    private static final Column[] COLUMNS = new Column[] { 
        new Column("id", DataType.INTEGER),
        new Column("name", DataType.VARCHAR), 
        new Column("description", DataType.VARCHAR),
        new Column("picture", DataType.BLOB), 
        new Column("occasion", DataType.VARCHAR),
        new Column("instruction", DataType.VARCHAR),
        new Column("nonalcoholic", DataType.BOOLEAN),
        new Column("bartool_id", DataType.INTEGER),
        new Column("glass_id", DataType.INTEGER)};
    private static final Column[] PRIMARY_KEYS = new Column[] { new Column("id", DataType.INTEGER) };
    
    private DrinkDao dao;
    private DaoPrepAndExpectedTestCase tc;
    
    @BeforeEach
    void setup() throws Exception {
        var vtd = new VerifyTableDefinition[] {
            new VerifyTableDefinition(TABLE_NAME, new String[] { "id" }),
            new VerifyTableDefinition("glasses", new String[] { "id" }),
            new VerifyTableDefinition("bartools", new String[] { "id" }),
            new VerifyTableDefinition("bottles", new String[] { "id" }),
            new VerifyTableDefinition("categories", new String[] { "id" }),
            new VerifyTableDefinition("ingredients", new String[] { "id", "drinkid" }),
            new VerifyTableDefinition("bots_cats", new String[] { }),
            new VerifyTableDefinition("drks_cats", new String[] { })
        };
        dao = new DrinkDaoJpa(getEMF().createEntityManager());
        tc = new DaoPrepAndExpectedTestCase(vtd);        
    }

    @DisplayName("get Drink by name")
    @Test
    void testGetByName() throws Exception {
        Optional<Drink> drink = (Optional<Drink>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getByName("Fruchti");
        });
        Drink d = drink.get();
        var desc = d.getDescription().get();
        var inst = d.getInstruction().get();
        var occa = d.getOccasion().get();
        var bart = d.getBarTool();
        var glas = d.getGlass();
        assertEquals("Fruchti", d.getName());
        assertEquals("Beschreibung Fruchti", desc);
        assertEquals("Gelegenheit Fruchti", occa);
        assertEquals("Rezept Fruchti", inst);
        assertEquals("Shaker", bart.getName());
        assertEquals("Longdrinkglas", glas.getName());
    }

    @DisplayName("get non-existing Drink by name")
    @Test
    void testGetNoneByName() throws Exception {
        Optional<Drink> drink = (Optional<Drink>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getByName("non-existing");
        });
        assertTrue(drink.isEmpty());
    }

    private int getPosFor(List<Category> cats, String name) {
        int pos = 0;
        Iterator<Category> it = cats.iterator();
        while (it.hasNext()) {
            Category cat = it.next();
            if (name.equals(cat.getName())) {
                return pos;
            }
            pos++;
        }
        return -1;
    }
    
    @DisplayName("get Drink by id")
    @Test
    void testGetById() throws Exception {
        Optional<Drink> drink = (Optional<Drink>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getById(102);
        });
        Drink d = drink.get();
        assertEquals("Mojito", d.getName());
        List<Category> cats = d.getCategories();
        assertEquals(2, cats.size());
        var pos1 = getPosFor(cats, "Longdrink");
        var pos2 = getPosFor(cats, "Brand aus Krautern");
        assertTrue(pos1 >= 0);
        assertTrue(pos2 >= 0);
        assertNotEquals(pos1, pos2);
    }

    @DisplayName("get non-existing Drink by id")
    @Test
    void testGetNoneById() throws Exception {
        Optional<Drink> drink = (Optional<Drink>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getById(4711);
        });
        assertTrue(drink.isEmpty());
    }

    @DisplayName("insert new Drink")
    @Test
    void testInsert() throws Exception {
        Glass glass = new Glass("Tumbler");
        setIdOf(glass, 1002L);
        
        BarTool bartool = new BarTool("Shaker");
        setIdOf(bartool, 2002L);
        
        Bottle bottle1 = new Bottle("Gin", 42);
        setIdOf(bottle1, 5007L);
        Bottle bottle2 = new Bottle("Grapefruitsaft", 0);
        setIdOf(bottle2, 5002L);
        
        Category category = new Category("Kinderdrink", "Drink");
        Class<?> clazz = category.getClass();
        Field field = clazz.getDeclaredField("id");
        field.setAccessible(true);
        field.set(category, 3001L);
        
        Drink drink = new Drink("Wodka Martini");
        drink.setDescription(Optional.of("James Bond Drink"));
        drink.setOccasion(Optional.of("im Casino"));
        drink.setInstruction(Optional.of("shaken, not stirred"));
        drink.setNonalcoholic(false);
        drink.setGlass(glass);
        drink.setBarTool(bartool);
        drink.addIngredient(bottle1, 5, Optional.of("ounce"));
        drink.addIngredient(bottle2, 6, Optional.of("spoons"));
        drink.addOrReplaceCategory(category);
        
        tc.runTest(DATA_LOAD, DATA_INSERT, () -> {
            dao.saveOrUpdate(drink);
            return null;
        });
    }

    @DisplayName("insert new Drink with non-existing bottle")
    @Test
    void testInsertNonExisting() throws Exception {
        Glass glass = new Glass("Tumbler");
        setIdOf(glass, 1002L);
        
        BarTool bartool = new BarTool("Shaker");
        setIdOf(bartool, 2002L);
        
        Bottle bottle1 = new Bottle("Gin", 42);
        setIdOf(bottle1, 5020L);
        Bottle bottle2 = new Bottle("Grapefruitsaft", 0);
        setIdOf(bottle2, 5002L);
        
        Drink drink = new Drink("Wodka Martini");
        drink.setDescription(Optional.of("James Bond Drink"));
        drink.setOccasion(Optional.of("im Casino"));
        drink.setInstruction(Optional.of("shaken, not stirred"));
        drink.setNonalcoholic(false);
        drink.setGlass(glass);
        drink.setBarTool(bartool);
        drink.addIngredient(bottle1, 5, Optional.of("ounce"));
        drink.addIngredient(bottle2, 6, Optional.of("spoons"));
        
        tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            assertThrows(DrinksStorageException.class, () -> {
                dao.saveOrUpdate(drink);
            });
            return null;
        });
    }

    @DisplayName("update a Drink")
    @Test
    void testUpdate() throws Exception {
        Glass glass = new Glass("Tumbler");
        setIdOf(glass, 1002L);
        
        BarTool bartool = new BarTool("Rührglas");
        setIdOf(bartool, 2001L);
        
        Drink drink = dao.getById(102).get();
        
        drink.setName("Mochito");
        drink.setDescription(Optional.of("Desc Mojito"));
        drink.setOccasion(Optional.of("Occa Mojito"));
        drink.setInstruction(Optional.of("Instr Mojito"));
        drink.setGlass(glass);
        drink.setBarTool(bartool);
        
        tc.runTest(DATA_LOAD, DATA_UPDATE, () -> {
            dao.saveOrUpdate(drink);
            return null;
        });
    }

    @DisplayName("update a Drink with non-existing Glass")
    @Test
    void testUpdateNonExisting() throws Exception {
        Glass glass = new Glass("Tumbler");
        setIdOf(glass, 1005L);
        
        BarTool bartool = new BarTool("Rührglas");
        setIdOf(bartool, 2001L);
        
        tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            assertThrows(DrinksStorageException.class, () -> {
                Drink drink = dao.getById(102).get();
                
                drink.setName("Mochito");
                drink.setDescription(Optional.of("Desc Mojito"));
                drink.setOccasion(Optional.of("Occa Mojito"));
                drink.setInstruction(Optional.of("Instr Mojito"));
                drink.setGlass(glass);
                drink.setBarTool(bartool);
                
                dao.saveOrUpdate(drink);
            });
            return null;
        });
    }

    @DisplayName("delete a Drink")
    @Test
    void testDelete() throws Exception {
        tc.runTest(DATA_LOAD, DATA_DELETE, () -> {
            dao.delete(102L);
            return null;
        });
    }

    @Override
    protected void fillTable(DefaultTable t, List<Drink> list) throws Exception {
        int row = 0;
        for (Drink d: list) {
            String desc = d.getDescription().isPresent() ? d.getDescription().get() : null;
            byte[] blob = d.getPicture().isPresent() ? d.getPicture().get() : null;
            String occa = d.getOccasion().isPresent() ? d.getOccasion().get() : null;
            String inst = d.getInstruction().isPresent() ? d.getInstruction().get() : null;
            t.addRow();
            t.setValue(row, "id", d.getId());
            t.setValue(row, "name", d.getName());
            t.setValue(row, "description", desc);
            t.setValue(row, "picture", blob);
            t.setValue(row, "occasion", occa);
            t.setValue(row, "instruction", inst);
            t.setValue(row, "nonalcoholic", d.isNonalcoholic());
            t.setValue(row, "bartool_id", d.getBarTool().getId());
            t.setValue(row, "glass_id", d.getGlass().getId());
            row++;        
        }
    }

    private void setIdOf(Object object, long value) throws Exception {
        Class<?> clazz = object.getClass().getSuperclass();
        Field field = clazz.getDeclaredField("id");
        field.setAccessible(true);
        field.set(object, value);    
    }
    
    @Override
    protected Column[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected Column[] getPrimaryKeys() {
        return PRIMARY_KEYS;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }
}
