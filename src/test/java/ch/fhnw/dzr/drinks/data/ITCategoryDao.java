package ch.fhnw.dzr.drinks.data;

import java.util.List;
import java.util.Optional;

import org.dbunit.Assertion;
import org.dbunit.VerifyTableDefinition;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DefaultTable;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.datatype.DataType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.fhnw.dzr.drinks.data.jpa.CategoryDaoJpa;
import ch.fhnw.dzr.drinks.model.Category;

public class ITCategoryDao extends DaoTest<Category> {

    private static final String DATA_LOAD = "CategoriesFull.xml";
    private static final String DATA_CATS_ONLY = "CategoriesOnly.xml";
    private static final String DATA_TYPES_ONLY = "CategoriesTypesOnly.xml";
    private static final String DATA_INSERT1 = "CategoriesInsert.xml";
    private static final String DATA_INSERT2 = "CategoriesInsert2.xml";
    private static final String DATA_UPDATE = "CategoriesUpdate.xml";
    private static final String DATA_DELETE1 = "CategoriesDelete1.xml";
    private static final String DATA_DELETE2 = "CategoriesDelete2.xml";
    
    private static final String TABLE_NAME = "CATEGORIES";
    private static final Column[] COLUMNS = new Column[] { 
        new Column("id", DataType.INTEGER),
        new Column("name", DataType.VARCHAR), 
        new Column("description", DataType.VARCHAR),
        new Column("type", DataType.VARCHAR) };
    private static final Column[] PRIMARY_KEYS = new Column[] { new Column("id", DataType.INTEGER) };

    private CategoryDao dao;
    private VerifyTableDefinition vtd;
    private DaoPrepAndExpectedTestCase tc;

    @BeforeEach
    void setup() throws Exception {
        dao = new CategoryDaoJpa(getEMF().createEntityManager());
        vtd = new VerifyTableDefinition(TABLE_NAME, new String[] { "id" });
        tc = new DaoPrepAndExpectedTestCase(vtd);
    }

    @SuppressWarnings("unchecked")
    @DisplayName("get all categories")
    @Test
    void testGetAllCategories() throws Exception {
        List<Category> cats = (List<Category>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getAllCategories();
        });

        ITable actualTable = convertToTable(cats);
        ITable expectedTable = this.getExpectedTable(DATA_CATS_ONLY);
        
        Assertion.assertEquals(expectedTable, actualTable);
    }
    
    @SuppressWarnings("unchecked")
    @DisplayName("get all types of given category")
    @Test
    void testGetAllTypes() throws Exception {
        List<Category> cats = (List<Category>) tc.runTest(DATA_LOAD, DATA_LOAD, () -> {
            return dao.getAllTypesOf("Drink");
        });

        ITable actualTable = convertToTable(cats);
        ITable expectedTable = this.getExpectedTable(DATA_TYPES_ONLY);
        
        Assertion.assertEquals(expectedTable, actualTable);        
    }
    
    @DisplayName("create new category an save it")
    @Test
    void testSaveCategory() throws Exception {
        Category cat = new Category("Fahrzeuge", "Fahrzeuge");
        Category typ = new Category("Fahrzeuge", "Auto");
        cat.setTypeDescription(Optional.of("hat vier Raeder"));
        
        tc.runTest(DATA_LOAD, DATA_INSERT1, () -> {
            dao.saveOrUpdateCategory(cat);
            return null;
        });
        
        tc.runTest(DATA_INSERT1, DATA_INSERT2, () -> {
            dao.saveOrUpdateCategory(typ);
            return null;
        });        
    }
    
    @DisplayName("change category name")
    @Test
    void testUpdateCategory() throws Exception {
        Category cat = dao.getAllCategories().get(0);
        
        cat.setName("Gugus");
        
        tc.runTest(DATA_LOAD, DATA_UPDATE, () -> {
            dao.saveOrUpdateCategory(cat);
            return null;
        });
    }
    
    @DisplayName("delete single type from a category")
    @Test
    void testDeleteCategoryType() throws Exception {
        tc.runTest(DATA_LOAD, DATA_DELETE1, () -> {
            dao.deleteCategory(1004);
            return null;
        });
    }

    @DisplayName("delete category")
    @Test
    void testDeleteCategory() throws Exception {
        tc.runTest(DATA_LOAD, DATA_DELETE2, () -> {
            dao.deleteCategory(1006);
            return null;
        });
    }

    @Override
    protected void fillTable(DefaultTable t, List<Category> list) throws Exception {
        int row = 0;
        for (Category c : list) {
            String desc = c.getTypeDescription().isPresent() ? c.getTypeDescription().get() : null;
            t.addRow();
            t.setValue(row, "id", c.getId());
            t.setValue(row, "name", c.getName());
            t.setValue(row, "description", desc);
            t.setValue(row, "type", c.getType());
            row++;
        }
    }

    @Override
    protected Column[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected Column[] getPrimaryKeys() {
        return PRIMARY_KEYS;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }
    
}
