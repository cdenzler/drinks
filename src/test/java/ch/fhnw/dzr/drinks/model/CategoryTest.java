package ch.fhnw.dzr.drinks.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Testing class Category")
class CategoryTest {

    @DisplayName("Test Ctor with correct arguments.")
    @Test
    void testCategory() {
        var cat = new Category("Kategorie", "Typ");
        assertEquals("Kategorie", cat.getName());
        assertEquals("Typ", cat.getType());
    }

    @DisplayName("Test Ctor with null arguments")
    @Test
    void testCategoryNull() {
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> {
            new Category(null, "Typ");
        });
        assertEquals(Category.NO_OR_EMPTY_STRING, e.getMessage());

        e = assertThrows(IllegalArgumentException.class, () -> {
            new Category("Kategorie", null);
        });
        assertEquals(Category.NO_OR_EMPTY_STRING, e.getMessage());
    }

    @DisplayName("Test Ctor with empty arguments")
    @Test
    void testCategoryEmpty() {
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> {
            new Category("", "Typ");
        });
        assertEquals(Category.NO_OR_EMPTY_STRING, e.getMessage());

        e = assertThrows(IllegalArgumentException.class, () -> {
            new Category("Kategorie", "");
        });
        assertEquals(Category.NO_OR_EMPTY_STRING, e.getMessage());
    }

    @DisplayName("Test mutator and accessor for description.")
    @Test
    public void testTypeDescription() {
        var cat = new Category("Kategorie", "Typ");
        assertTrue(cat.getTypeDescription().isEmpty());
        cat.setTypeDescription(Optional.of("blabla"));
        assertEquals(Optional.of("blabla"), cat.getTypeDescription());
        cat.setTypeDescription(Optional.empty());
        assertTrue(cat.getTypeDescription().isEmpty());
    }

}
