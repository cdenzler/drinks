package ch.fhnw.dzr.drinks.model;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Unit testing the common abstract base class Describable.
 * 
 * @see Describable
 */
@DisplayName("Testing abstract class Describable")
abstract class DescribableTest {

    private byte[] picture;
    
    protected abstract Describable create(String name);

    /**
     * Test mutator and accessor for name. Makes sure that:
     * <ul>
     * <li>the name which is set, can be retrieved unchanged.
     * <li>an exception is thrown when trying to set a null value.
     * <li>an exception is thrown when trying to set an empty string.
     * </ul>
     */
    @DisplayName("Test correct handling of null and empty Strings of the name property")
    @Test
    void testName() {
        Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
            create(null);
        });
        assertEquals(Describable.NO_OR_EMPTY_STRING, ex.getMessage());

        ex = assertThrows(IllegalArgumentException.class, () -> {
            create("");
        });
        assertEquals(Describable.NO_OR_EMPTY_STRING, ex.getMessage());
    }

    @DisplayName("Test mutator and accessor for description.")
    @Test
    public void testDescription() {
    	Describable desc = create("description");
    	assertEquals("description", desc.getName());
        desc.setDescription(Optional.of("blabla"));
        assertEquals(Optional.of("blabla"), desc.getDescription());
        desc.setDescription(Optional.empty());
        assertTrue(desc.getDescription().isEmpty());
    }

    @DisplayName("Test getter and setter of picture.")
    @Test
    void testPicture() throws Exception {
    	Describable desc = create("description");
        picture = "Hallo Welt, wie geht es Dir?".getBytes();
        desc.setPicture(Optional.of(picture));

        byte[] actual = desc.getPicture().get();
        assertEquals(28, actual.length);
        assertArrayEquals(picture, actual);
    }

    @DisplayName("Test if null picture is stored if byte array to small.")
    @Test
    void testNoPicture() {
    	Describable desc = create("description");
        picture = new byte[0];
        desc.setPicture(Optional.of(picture));
        assertTrue(desc.getPicture().isEmpty());
    }

    @DisplayName("Test if null picture is stored and overrides old entry if byte array is to small.")
    @Test
    void overridePictureCorrectly() throws Exception {
    	Describable desc = create("description");
        picture = "Hallo Welt, wie geht es Dir?".getBytes();
        desc.setPicture(Optional.of(picture));

        picture = new byte[0];
        desc.setPicture(Optional.of(picture));
        assertTrue(desc.getPicture().isEmpty());
    }

    @DisplayName("Test if null is also stored correctly.")
    @Test
    void overridePictureWithNull() throws Exception {
    	Describable desc = create("description");
        picture = "Hallo Welt, wie geht es Dir?".getBytes();
        desc.setPicture(Optional.of(picture));

        desc.setPicture(Optional.empty());
        assertTrue(desc.getPicture().isEmpty());
    }

    @DisplayName("Test equals")
    @Test
    void testEquals() {
        Describable desc = create("description");
        assertTrue(desc.equals(desc));
        assertFalse(desc.equals(null));
        assertFalse(desc.equals("Hallo"));
        Describable desc2 = create("description");
        assertEquals(desc, desc2);
        Describable desc3 = create("blabla");
        assertNotEquals(desc, desc3);
    }
    
    @DisplayName("Test hashCode")
    @Test
    void testHash() {
        var desc1 = create("hello");
        var desc2 = create("hello");
        var desc3 = create("world");
        assertEquals(desc1.hashCode(), desc2.hashCode());
        assertNotEquals(desc1.hashCode(), desc3.hashCode());
    }
    
}
