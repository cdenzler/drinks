package ch.fhnw.dzr.drinks.model;

import org.junit.jupiter.api.DisplayName;

/**
 * Unit tests the class glass.
 */
@DisplayName("Testing class Glass")
class GlassTest extends DescribableTest {

    @Override
    protected Glass create(String name) {
        return new Glass(name);
    }

}
