package ch.fhnw.dzr.drinks.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Unit testing the Ingredient class.
 * 
 * @see Ingredient
 */
@DisplayName("Testing class Ingredients")
public class IngredientTest {

    private Ingredient ingr;
    private Drink drink;
    private Bottle bottle;
    
    @BeforeEach
    void setup() {
        drink = mock(Drink.class);
        bottle = mock(Bottle.class);
        when(bottle.getName()).thenReturn("aName");
    }

    @DisplayName("Test constructor with valid parameters.")
    @Test
    void testConstructor() {
        ingr = new Ingredient(drink, bottle, 43.24, Optional.of("cl"));
        assertEquals("aName", ingr.getName());
        assertEquals(43.24, ingr.getQuantity(), 1e-10);
        assertEquals(Optional.of("cl"), ingr.getUnit());
    }

    @DisplayName("Test constructor with negative number as quantity parameter.")
    @Test
    void testConstructorInvalidQuantity() {
        Ingredient ingr = new Ingredient(drink, bottle, 1e-10, Optional.of("cl"));
        assertEquals(1e-10, ingr.getQuantity(), 1e-11);

        assertThrows(IllegalArgumentException.class, () -> {
            new Ingredient(drink, bottle, -1e10, Optional.of("cl"));
        });

        assertThrows(IllegalArgumentException.class, () -> {
            new Ingredient(drink, bottle, -235642.2135, Optional.of("cl"));
        });
    }

    @DisplayName("Test constructor with null as unit parameter.")
    @Test
    void testConstructorNoUnit() {
        Ingredient ingredient = new Ingredient(drink, bottle, 43.24, Optional.empty());
        assertTrue(ingredient.getUnit().isEmpty());
    }

    @DisplayName("Test new definition of equals.")
    @Test
    void testEquals() {
        Ingredient ingr = new Ingredient(drink, bottle, 42.23, Optional.of("dash"));
        assertTrue(ingr.equals(ingr));

        assertFalse(ingr.equals("aName"));

        Ingredient ingr2 = new Ingredient(drink, bottle, 23.0, Optional.of("dash"));
        assertTrue(ingr.equals(ingr2));

        ingr2 = new Ingredient(drink, bottle, 42.23, Optional.of("cl"));
        assertTrue(ingr.equals(ingr2));

        ingr2 = new Ingredient(drink, bottle, 42.23, Optional.of("dash"));
        assertTrue(ingr.equals(ingr2));
    }
}
