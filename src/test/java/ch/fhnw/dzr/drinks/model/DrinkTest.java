package ch.fhnw.dzr.drinks.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Unit testing the Drink class.
 * 
 * @see Drink
 */
@DisplayName("Testing class Drink")
public class DrinkTest {

    private Drink drink;
    private Bottle[] bottles = new Bottle[4];

    /**
     * Creates an object of the class under test.
     * 
     * @throws Exception if anything goes wrong.
     */
    @BeforeEach
    public void setUp() throws Exception {
        drink = new Drink("aName");
        for (int i = 0; i < 4; i++) {
            bottles[i] = mock(Bottle.class);
            when(bottles[i].getName()).thenReturn("bottle" + i);
        }
    }

    /**
     * Test mutator and accessor for occasion. Makes sure that:
     * <ul>
     * <li>the occasion which is set, can be retrieved unchanged.
     * <li>an empty String is returned when set to null.
     * </ul>
     */
    @DisplayName("Test mutator and accessor for occasion.")
    @Test
    public void testOccasion() {
        drink.setOccasion(Optional.of("anOccasion"));
        assertEquals(Optional.of("anOccasion"), drink.getOccasion());

        drink.setOccasion(Optional.empty());
        assertEquals(Optional.empty(), drink.getOccasion());
    }

    /**
     * Test mutator and accessor for instruction. Makes sure that:
     * <ul>
     * <li>the instructions which are set, can be retrieved unchanged.
     * <li>an empty String is returned when set to null.
     * </ul>
     */
    @DisplayName("Test mutator and accessor for instruction.")
    @Test
    public void testInstruction() {
        drink.setInstruction(Optional.of("someInstructions"));
        assertEquals(Optional.of("someInstructions"), drink.getInstruction());

        drink.setInstruction(Optional.empty());
        assertEquals(Optional.empty(), drink.getInstruction());
    }

    private Ingredient[] createIngredientsArray(Drink drink) {
        // IMPORTANT: using real objects, as we rely on the special equals implementation.
        Ingredient[] ingrs = new Ingredient[4];
        for (int i = 0; i < 4; i++) {
            ingrs[i] = new Ingredient(drink, bottles[i], i + 1.0, Optional.of("cl"));
        }
        return ingrs;
    }

    /**
     * Test adding ingredient, using real Ingredients objects. Makes sure that:
     * <ul>
     * <li>the list of all ingredients obtained from a Drinks object is immutable.
     * <li>adding new ingredients works as expected.
     * </ul>
     */
    @Test
    public void testAddIngredient() {
        Ingredient[] allIngrs = drink.getIngredients();
        assertEquals(0, allIngrs.length);

        Ingredient[] ingrs = createIngredientsArray(drink);
        // add elements to ingredient list
        
        for (int i = 0; i < 4; i++) {
            var bottle = ingrs[i].getBottle();
            var qty = ingrs[i].getQuantity();
            var unit = ingrs[i].getUnit();
            drink.addIngredient(bottle, qty, unit);
        }
        
        allIngrs = drink.getIngredients();
        assertEquals(4, allIngrs.length);
        
        for (int i = 0; i < 4; i++) {
            assertEquals(ingrs[i], allIngrs[i]); // Note: only compares names of bottles.
        }
    }

    /**
     * Test replacing ingredients, using real Ingredients objects.
     */
    @Test
    public void testReplaceIngredients() {
        // add some ingredients
        Ingredient[] ingrs = createIngredientsArray(drink);
        for (int i = 0; i < 4; i++) {
            var bottle = ingrs[i].getBottle();
            var qty = ingrs[i].getQuantity();
            var unit = ingrs[i].getUnit();
            drink.addIngredient(bottle, qty, unit);
        }

        // check size before update
        Ingredient[] allIngrs = drink.getIngredients();
        int sizeBeforeUpdate = allIngrs.length;
        assertEquals(ingrs.length, sizeBeforeUpdate);

        // check entry 2
        Ingredient ingr = ingrs[2];
        assertEquals(ingr, allIngrs[2]);
        assertEquals(ingr.getQuantity(), allIngrs[2].getQuantity());
        
        // change entry 2
        Bottle bottle = mock(Bottle.class);
        when(bottle.getName()).thenReturn("bottle2");
        drink.addIngredient(bottle, 42.25, Optional.of("cl"));
        
        // read all ingredients again and compare expected with actual
        allIngrs = drink.getIngredients();
        assertEquals(ingrs.length, allIngrs.length);
        ingr = allIngrs[2];
        assertEquals("bottle2", ingr.getName());
        assertEquals(42.25, ingr.getQuantity());        
    }

    /**
     * Test removing ingredients, using real Ingredients objects.
     */
    @Test
    public void testRemoveIngredients() {
        // add some ingredients
        Ingredient[] ingrs = createIngredientsArray(drink);
        for (int i = 0; i < 4; i++) {
            var bottle = ingrs[i].getBottle();
            var qty = ingrs[i].getQuantity();
            var unit = ingrs[i].getUnit();
            drink.addIngredient(bottle, qty, unit);
        }

        // check size before removal
        Ingredient[] allIngrs = drink.getIngredients();
        int sizeBeforeRemoval = allIngrs.length;
        assertEquals(ingrs.length, sizeBeforeRemoval);

       // remove non-existing ingredient and make sure nothing was deleted
        // ingredients equal only on their name
        Bottle bottle = mock(Bottle.class);
        when(bottle.getName()).thenReturn("non-existent");
        drink.removeIngredient(bottle);
        assertEquals(sizeBeforeRemoval, drink.getIngredients().length);
        
        // remove existing ingredient and make sure nothing else was deleted
        // we use real Ingredient object because of its equals implementation
        bottle = mock(Bottle.class);
        when(bottle.getName()).thenReturn("bottle1");
        drink.removeIngredient(bottle);
        
        allIngrs = drink.getIngredients();
        assertEquals(sizeBeforeRemoval - 1, allIngrs.length);
        for (Ingredient i: allIngrs) {
            assertNotEquals("bottle1", i.getName());
        }
    }

    /**
     * Test clearing ingredient list.
     */
    @Test
    public void testClearIngredients() {
        assertEquals(0, drink.getIngredients().length);
        // add some ingredients
        Ingredient[] ingrs = createIngredientsArray(drink);
        for (int i = 0; i < 4; i++) {
            var bottle = ingrs[i].getBottle();
            var qty = ingrs[i].getQuantity();
            var unit = ingrs[i].getUnit();
            drink.addIngredient(bottle, qty, unit);
        }

        assertEquals(ingrs.length, drink.getIngredients().length);
        // call clear on non-empty list
        drink.clearIngredients();
        assertEquals(0, drink.getIngredients().length);
        // call clear on empty list
        drink.clearIngredients();
        assertEquals(0, drink.getIngredients().length);
    }

    /**
     * Test mutator and accessor for utensil. Makes sure that: the utensil which is set, can be
     * retrieved unchanged.
     */
    @Test
    public void testUtensils() {
        BarTool bartool = mock(BarTool.class);
        assertNull(drink.getBarTool());

        drink.setBarTool(bartool);
        assertEquals(bartool, drink.getBarTool());
    }

    /**
     * Test mutator and accessor for glass. Makes sure that: the glass which is set, can be
     * retrieved unchanged.
     */
    @Test
    public void testGlass() {
        Glass glass = mock(Glass.class);
        assertNull(drink.getGlass());

        drink.setGlass(glass);
        assertEquals(glass, drink.getGlass());
    }

    /**
     * Test mutator and accessor for nonalcoholic. Makes sure that:
     * <ul>
     * <li>a drink is initially alcoholic.
     * <li>the status which is set can be retrieved unchanged.
     * </ul>
     */
    @Test
    public void testNonalcoholic() {
        assertFalse(drink.isNonalcoholic());

        drink.setNonalcoholic(true);
        assertTrue(drink.isNonalcoholic());

        drink.setNonalcoholic(false);
        assertFalse(drink.isNonalcoholic());
    }

    /** Test mutator and accessor for category. */
    public void testCategory() {
        Category cat = mock(Category.class);
        assertEquals(0, drink.getCategories().size());

        drink.addOrReplaceCategory(cat);
        assertEquals(1, drink.getCategories().size());
    }
}
