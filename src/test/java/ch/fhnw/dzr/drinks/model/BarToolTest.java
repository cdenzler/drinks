package ch.fhnw.dzr.drinks.model;

import org.junit.jupiter.api.DisplayName;

/**
 * Unit tests the class BarTool.
 */
@DisplayName("Testing class BarTool")
class BarToolTest extends DescribableTest {

    @Override
    protected BarTool create(String name) {
        return new BarTool(name);
    }

}
