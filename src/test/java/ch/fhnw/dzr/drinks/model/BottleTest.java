package ch.fhnw.dzr.drinks.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * Unit testing the Bottle class.
 * 
 * @see Bottle
 */
@DisplayName("Testing class Bottle")
public class BottleTest {

    private Bottle bottle;

    /**
     * Creates an object of the class under test.
     * 
     * @throws Exception if anything goes wrong.
     */
    @BeforeEach
    void setUp() throws Exception {
        bottle = new Bottle("aName", 42);
    }

    /**
     * Test mutator and accessor for price. Makes sure that:
     * <ul>
     * <li>the price is initially set to 0.0f.
     * <li>the price which is set can be retrieved unchanged.
     * <li>an exception is thrown when trying to set a negative price.
     * </ul>
     */
    @Test
    void testGetPrice() {
        assertTrue(bottle.getPrice().isEmpty()); // exact, no delta!

        Optional<Double> price = Optional.of(42.4711);
        bottle.setPrice(price);
        assertEquals(price, bottle.getPrice()); // no change, no delta!

        // Boundary cases
        price = Optional.of(0.0001);
        bottle.setPrice(price);
        assertEquals(price, bottle.getPrice()); // no change, no delta!

        price = Optional.of(0.0);
        bottle.setPrice(price);
        assertEquals(price, bottle.getPrice()); // no change, no delta!

        price = Optional.of(-0.0);
        bottle.setPrice(price);
        assertEquals(price, bottle.getPrice()); // no change, no delta!

        Throwable ex = assertThrows(IllegalArgumentException.class, () -> {
            bottle.setPrice(Optional.of(-0.001));
        });
        assertEquals(Bottle.NEGATIVE_PRICE, ex.getMessage());

        // Representative case for equivalence class
        ex = assertThrows(IllegalArgumentException.class, () -> {
            bottle.setPrice(Optional.of(-5.3452345325e15));
        });
        assertEquals(Bottle.NEGATIVE_PRICE, ex.getMessage());
    }
    
    @DisplayName("make sure that Bottle is initialized with legal ABV")
    @Test
    void testInitWithABV() {
        assertEquals(42, bottle.getABV());        
    }

    @DisplayName("test ABV with legal values")
    @ParameterizedTest
    @ValueSource(ints = {0, 1, 42, 99, 100})
    void testGetABV(int abv) {
        bottle.setABV(abv);
        assertEquals(abv, bottle.getABV());
    }
    
    @DisplayName("test exceptions when setting illegal ABV value.")
    @ParameterizedTest
    @ValueSource(ints = {-205, -1, 101, 12035})
    void exceptionsOnABV(int abv) {
        var ex = assertThrows(IllegalArgumentException.class, () -> {
            bottle.setABV(abv);
        });
        assertEquals(Bottle.WRONG_ALCOHOL_VOL, ex.getMessage());
    }

    @DisplayName("test inStock, initially bottle is never in stock.")
    @Test
    void testInStock() {
        assertFalse(bottle.isInStock());

        bottle.setInStock(true);
        assertTrue(bottle.isInStock());

        bottle.setInStock(false);
        assertFalse(bottle.isInStock());
    }
    
    @DisplayName("test categories")
    @Test
    void testCategories() {
        var cat1 = mock(Category.class);
        var cat2 = mock(Category.class);
        when(cat1.getName()).thenReturn("one");
        when(cat2.getName()).thenReturn("two");
        
        bottle.addCategory(cat1);
        bottle.addCategory(cat2);
        
        
    }
}
