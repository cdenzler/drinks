/**
 * The base package for the Drinks! REST server application.
 */
package ch.fhnw.dzr.drinks;
