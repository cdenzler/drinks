package ch.fhnw.dzr.drinks.data.jpa;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import ch.fhnw.dzr.drinks.data.BottleDao;
import ch.fhnw.dzr.drinks.model.Bottle;
import ch.fhnw.dzr.drinks.model.DrinksStorageException;

public class BottleDaoJpa implements BottleDao {
    /** Query to get all bottles. */
    private static final String GET_ALL_BOTTLES = "SELECT b FROM Bottle b";
    /** Query to get bottles with a maximum alcohol level. */
    private static final String GET_BOTTLES_BY_ALCOHOL = "SELECT b FROM Bottle b WHERE abv <= :mxabv";
    /** Query to get bottles which are in stock (or not). */
    private static final String GET_BOTTLES_BY_STOCK = "SELECT b FROM Bottle b WHERE instock = :instock";
    /** Query a bottle by its name. */
    private static final String GET_BOTTLES_BY_NAME = "SELECT b FROM Bottle b WHERE b.name LIKE :name";
    /** Query a bottle by its name ignoring case. */
    private static final String GET_BOTTLE_BY_NAME_IGNORECASE =
            "SELECT b FROM Bottle b WHERE LOWER( b.name ) LIKE :name";

    private EntityManager em;

    /**
     * Create a data access object (DAO) for bottle related database queries. This DAO uses JPA.
     * 
     * @param em the EntityManager to use with this DAO.
     */
    public BottleDaoJpa(EntityManager em) {
        this.em = em;
    }

    public List<Bottle> getAllBottles() {
        TypedQuery<Bottle> query = em.createQuery(GET_ALL_BOTTLES, Bottle.class);
        List<Bottle> result = query.getResultList();
        return result;
    }

    @Override
    public Optional<Bottle> getBottleByName(String name) {
        TypedQuery<Bottle> query = em.createQuery(GET_BOTTLES_BY_NAME, Bottle.class).setParameter("name", name);
        Bottle result;
        try {
            result = query.getSingleResult();
        } catch (NoResultException e) {
            result = null;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public List<Bottle> getBottlesByName(String name, boolean containsMatch, boolean ignorecase) {
        final String queryString;
        if (ignorecase) {
            queryString = GET_BOTTLE_BY_NAME_IGNORECASE;
            name = name.toLowerCase();
        } else {
            queryString = GET_BOTTLES_BY_NAME;
        }
        if (containsMatch) {
            name = "%" + name + "%";
        }
        TypedQuery<Bottle> query = em.createQuery(queryString, Bottle.class).setParameter("name", name);
        List<Bottle> result = query.getResultList();
        return result;
    }

    @Override
    public List<Bottle> getBottleByABV(int maximumVolumeAlcohol) {
        TypedQuery<Bottle> query = em.createQuery(GET_BOTTLES_BY_ALCOHOL, Bottle.class);
        query.setParameter("mxabv", maximumVolumeAlcohol);
        em.getTransaction().begin();
        List<Bottle> result = query.getResultList();
        em.getTransaction().commit();
        return result;
    }

    @Override
    public List<Bottle> getBottleByAvailability(boolean isInStock) {
        TypedQuery<Bottle> query =
                em.createQuery(GET_BOTTLES_BY_STOCK, Bottle.class).setParameter("instock", isInStock);
        List<Bottle> result = query.getResultList();
        return result;
    }

    @Override
    public void saveOrUpdate(Bottle bottle) {
        executeInsideTransaction(em -> {
            if (bottle.getId() != 0) {
                em.merge(bottle);
            } else {
                em.persist(bottle);
            }
        });
    }

    @Override
    public void delete(Bottle bottle) {
        executeInsideTransaction(em -> em.remove(em.merge(bottle)));
    }

    private void executeInsideTransaction(Consumer<EntityManager> action) {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            action.accept(em);
            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw new DrinksStorageException(e);
        }
    }

}
