package ch.fhnw.dzr.drinks.data.jpa;

import java.util.Optional;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import ch.fhnw.dzr.drinks.data.DrinkDao;
import ch.fhnw.dzr.drinks.model.Drink;
import ch.fhnw.dzr.drinks.model.DrinksStorageException;

public class DrinkDaoJpa implements DrinkDao {

    /** Query a drink by its name. */
    private static final String GET_DRINK_BY_NAME = "SELECT d FROM Drink d WHERE LOWER(d.name) LIKE LOWER(:name)";
    /** Query to delete a drink by id. */
    private static final String DELETE_BY_ID = "DELETE FROM Drink d WHERE d.id = :id";

    private EntityManager em;

    /**
     * Create a data access object (DAO) for drink related database queries. This DAO uses JPA.
     * 
     * @param em the EntityManager to use with this DAO.
     */
    public DrinkDaoJpa(EntityManager em) {
        this.em = em;
    }

    @Override
    public Optional<Drink> getByName(String name) {
        TypedQuery<Drink> query = em.createQuery(GET_DRINK_BY_NAME, Drink.class).setParameter("name", name);
        Drink result;
        try {
            result = query.getSingleResult();
        } catch (NoResultException e) {
            result = null;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<Drink> getById(long id) {
        return Optional.ofNullable(em.find(Drink.class, id));
    }

    @Override
    public Drink saveOrUpdate(Drink drink) {
        executeInsideTransaction(em -> {
            if (drink.getId() != 0) {
                em.merge(drink);
            } else {
                em.persist(drink);
            }
        });
        return drink;
    }

    @Override
    public void delete(long id) {
        executeInsideTransaction(em -> {
            em.remove(em.find(Drink.class, id));
//            em.createQuery(DELETE_BY_ID).setParameter("id", id).executeUpdate();
        });
    }

    private void executeInsideTransaction(Consumer<EntityManager> action) {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            action.accept(em);
            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw new DrinksStorageException(e);
        }
    }
}
