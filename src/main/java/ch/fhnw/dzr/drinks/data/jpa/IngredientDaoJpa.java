package ch.fhnw.dzr.drinks.data.jpa;

import java.util.List;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import ch.fhnw.dzr.drinks.data.IngredientDao;
import ch.fhnw.dzr.drinks.model.DrinksStorageException;
import ch.fhnw.dzr.drinks.model.Ingredient;

public class IngredientDaoJpa implements IngredientDao {
    private EntityManager em;

    /**
     * Create a data access object (DAO) for ingredient related database queries. This DAO uses JPA.
     * 
     * @param em the EntityManager to use with this DAO.
     */
    public IngredientDaoJpa(EntityManager em) {
        this.em = em;
    }

    // Queries
    /** Query to get Ingredients for a given drink. */
    private static final String GET_INGREDIENTS_FOR = "SELECT i FROM Ingredient i WHERE i.drink.id = :id";
    /** Query to delete an Ingredient. */
    private static final String DELETE_INGREDIENTS = "DELETE Ingredient i WHERE i.drink.id = :id";

    @Override
    public List<Ingredient> getIngredientsFor(long drinkid) {
        var query = em.createQuery(GET_INGREDIENTS_FOR, Ingredient.class);
        query.setParameter("id", drinkid);
        return query.getResultList();
    }

    @Override
    public void deleteAllFor(long drinkId) {
        executeInsideTransaction(em -> {
            em.createQuery(DELETE_INGREDIENTS).setParameter("id", drinkId).executeUpdate();
        });
    }

// TODO: uncomment when Drinks class is implemented.
    @Override
    public void saveAllFor(long drinkId, List<Ingredient> ings) {
//        executeInsideTransaction(em -> {
//            em.createQuery(DELETE_INGREDIENTS).setParameter("id", drinkId).executeUpdate();
//            for (Ingredient i: ings) {
//                em.persist(i);
//            }
//        });
    }

    private void executeInsideTransaction(Consumer<EntityManager> action) {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            action.accept(em);
            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw new DrinksStorageException(e);
        }
    }
}
