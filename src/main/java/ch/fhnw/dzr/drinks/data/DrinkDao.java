package ch.fhnw.dzr.drinks.data;

import java.util.Optional;

import ch.fhnw.dzr.drinks.model.Drink;

/**
 * Provides data for drinks.
 */
public interface DrinkDao {

    /**
     * Retrieves a Drink object by the name of the drink.
     * 
     * @param name the name of the drink.
     * @return a drink object with it's ingredients.
     */
    Optional<Drink> getByName(String name);

    /**
     * Get Drink by its id.
     * 
     * @param id of the drink.
     * @return the Drink found with the given id or null if id did not exist.
     */
    Optional<Drink> getById(long id);

    /**
     * Saves or updates a Drink object. If it is a new object, then it is inserted otherwise it is
     * updated.
     * 
     * @param drink the Drink object to persist.
     * @return the persisted Drink, including an ID if it was a new drink.
     */
    Drink saveOrUpdate(Drink drink);

    /**
     * Delete a drink from the database.
     * 
     * @param id of the drink to delete.
     */
    void delete(long id);

}
