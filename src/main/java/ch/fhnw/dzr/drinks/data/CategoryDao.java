package ch.fhnw.dzr.drinks.data;

import java.util.List;

import ch.fhnw.dzr.drinks.model.Category;

/**
 * Provides names, descriptions and pictures of categories of different kinds.
 */
public interface CategoryDao {

    /**
     * Retrieves all categories.
     * 
     * @return All categories available.
     */
    List<Category> getAllCategories();

    /**
     * Retrieve all types of a category.
     * 
     * @param category the category of which to retrieve all types.
     * @return a list of types of the given category.
     */
    List<Category> getAllTypesOf(String category);

    /**
     * Write a type of a category to database no matter if it is a new object or an update of an
     * existing one.
     * 
     * @param cat the Category to persist.
     */
    void saveOrUpdateCategory(Category cat);

    /**
     * Delete a category with the given id. The id could refer to a type or a category. In the
     * latter case all types of the category will be deleted as well.
     * 
     * @param id the id of the category to delete. Note this can be a type or a category.
     */
    void deleteCategory(long id);

}
