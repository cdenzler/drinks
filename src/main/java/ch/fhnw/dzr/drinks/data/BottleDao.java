package ch.fhnw.dzr.drinks.data;

import ch.fhnw.dzr.drinks.model.Bottle;

import java.util.List;
import java.util.Optional;

/**
 * Provides data for bottles. 
 */
public interface BottleDao {

    /**
     * Read all bottles from database.
     * 
     * @return a list containing all bottles.
     */
    List<Bottle> getAllBottles();

    /**
     * Retrieve the Bottle with the given name.
     * 
     * @param name the name of the bottle.
     * @return the bottle with the given name or null if none was found.
     */
    Optional<Bottle> getBottleByName(String name);

    /**
     * Read only bottles with given name.
     * 
     * @param name the name of the bottles to search for. May contain %-wildcards.
     * @param containsMatch if true, performs a "contains name" query, i.e. pads the name with %-wildcards.
     * @param ignorecase if true, performs a case insensitive match on names.
     * @return a list of bottles that match the given name.
     */
    List<Bottle> getBottlesByName(String name, boolean containsMatch, boolean ignorecase);

    /**
     * Retrieve only bottles with given alcohol by volume.
     * 
     * @param maximumABV the maximum level of alcohol to be found in the bottles. Use 0
     *            to find non-alcoholic drinks.
     * @return a list of bottles which have not more than given level of alcohol.
     */
    List<Bottle> getBottleByABV(int maximumABV);

    /**
     * Retrieve bottles by their availability in stock.
     * 
     * @param isInStock if the bottle shall be in stock.
     * @return a list of bottles which are in stock (or not).
     */
    List<Bottle> getBottleByAvailability(boolean isInStock);

    /**
     * Write a Bottle object to database no matter if it is a new object or not.
     * 
     * @param bottle the bottle to persist.
     */
    void saveOrUpdate(Bottle bottle);

    /**
     * Delete a Bottle with the given name.
     * 
     * @param bottle the bottle to delete.
     */
    void delete(Bottle bottle);
}
