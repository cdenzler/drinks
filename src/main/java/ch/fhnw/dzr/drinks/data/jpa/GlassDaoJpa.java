package ch.fhnw.dzr.drinks.data.jpa;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import ch.fhnw.dzr.drinks.data.GlassDao;
import ch.fhnw.dzr.drinks.model.DrinksStorageException;
import ch.fhnw.dzr.drinks.model.Glass;

public class GlassDaoJpa implements GlassDao {
    private EntityManager em;

    /**
     * Create a data access object (DAO) for glass related database queries. This DAO uses JPA.
     * 
     * @param em the EntityManager to use with this DAO.
     */
    public GlassDaoJpa(EntityManager em) {
        this.em = em;
    }

    // Queries
    /** Query to get all glasses. */
    private static final String GET_ALL_GLASSES = "SELECT g FROM Glass g";
    /** Query a glass by its name. */
    private static final String GET_GLASSES_BY_NAME = "SELECT g FROM Glass g WHERE g.name LIKE :name";
    /** Delete a glass by its name. */
    private static final String DELETE_GLASSES_BY_NAME = "DELETE Glass g WHERE g.name = :name";

    @Override
    public List<Glass> getAllGlasses(boolean withPictures) {
        TypedQuery<Glass> query = em.createQuery(GET_ALL_GLASSES, Glass.class);
        List<Glass> result = query.getResultList();
        return result;
    }

    @Override
    public Optional<Glass> getGlassByName(String name) {
        TypedQuery<Glass> query = em.createQuery(GET_GLASSES_BY_NAME, Glass.class).setParameter("name", name);
        Glass result;
        try {
            result = query.getSingleResult();
        } catch (NoResultException e) {
            result = null;
        }
        return Optional.ofNullable(result);
    }

    private void executeInsideTransaction(Consumer<EntityManager> action) {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            action.accept(em);
            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw new DrinksStorageException(e);
        }
    }

    @Override
    public void saveOrUpdateGlass(Glass g) {
        executeInsideTransaction(em -> {
            if (g.getId() != 0) {
                em.merge(g);
            } else {
                em.persist(g);
            }
        });
    }

    @Override
    public void deleteGlass(String name) {
        executeInsideTransaction(em -> {
            Query q = em.createQuery(DELETE_GLASSES_BY_NAME).setParameter("name", name);
            q.executeUpdate();
        });
    }

}
