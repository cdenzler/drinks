package ch.fhnw.dzr.drinks.data;

import java.util.List;

import ch.fhnw.dzr.drinks.model.Ingredient;

/**
 * Provides data for ingredients.
 */
public interface IngredientDao {

    /**
     * Read all ingredients for a given drink from database.
     * 
     * @param drinkid the drink.
     * @return a list containing all ingredients.
     */
    List<Ingredient> getIngredientsFor(long drinkid);

    /**
     * Deletes all ingredients with the given drinkid.
     * 
     * @param drinkId the Drink for which all ingredients shall be deleted.
     */
    void deleteAllFor(long drinkId);

    /**
     * Update ingredients for a drink. All ingredients will be deleted and then a new list of
     * ingredients is inserted.
     * 
     * @param drinkId the Drink for which new ingredients are specified.
     * @param ings a list of ingredients to persist.
     */
    void saveAllFor(long drinkId, List<Ingredient> ings);

}
