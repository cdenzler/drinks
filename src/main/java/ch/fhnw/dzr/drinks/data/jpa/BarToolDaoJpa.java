package ch.fhnw.dzr.drinks.data.jpa;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import ch.fhnw.dzr.drinks.data.BarToolDao;
import ch.fhnw.dzr.drinks.model.DrinksStorageException;
import ch.fhnw.dzr.drinks.model.BarTool;

public class BarToolDaoJpa implements BarToolDao {
    private EntityManager em;

    /**
     * Create a data access object (DAO) for glass related database queries. This DAO uses JPA.
     * 
     * @param em the EntityManager to use with this DAO.
     */
    public BarToolDaoJpa(EntityManager em) {
        this.em = em;
    }

    // Queries
    /** Query to get all glasses. */
    private static final String GET_ALL_BARTOOLS = "SELECT b FROM BarTool b";
    /** Query a glass by its name. */
    private static final String GET_BARTOOL_BY_NAME = "SELECT b FROM BarTool b WHERE b.name LIKE :name";
    /** Delete a glass by its name. */
    private static final String DELETE_BARTOOL_BY_NAME = "DELETE BarTool b WHERE b.name = :name";

    @Override
    public List<BarTool> getAllBarTools(boolean withPictures) {
        TypedQuery<BarTool> query = em.createQuery(GET_ALL_BARTOOLS, BarTool.class);
        List<BarTool> result = query.getResultList();
        return result;
    }

    @Override
    public Optional<BarTool> getBarToolByName(String name) {
        TypedQuery<BarTool> query = em.createQuery(GET_BARTOOL_BY_NAME, BarTool.class).setParameter("name", name);
        BarTool result;
        try {
            result = query.getSingleResult();
        } catch (NoResultException e) {
            result = null;
        }
        return Optional.ofNullable(result);
    }

    private void executeInsideTransaction(Consumer<EntityManager> action) {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            action.accept(em);
            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw new DrinksStorageException(e);
        }
    }

    @Override
    public void saveOrUpdateBarTool(BarTool u) {
        executeInsideTransaction(em -> {
            if (u.getId() != 0) {
                em.merge(u);
            } else {
                em.persist(u);
            }
        });
    }

    @Override
    public void deleteBarTool(String name) {
        executeInsideTransaction(em -> {
            Query q = em.createQuery(DELETE_BARTOOL_BY_NAME).setParameter("name", name);
            q.executeUpdate();
        });
    }

}
