package ch.fhnw.dzr.drinks.data.jpa;

import java.util.List;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import ch.fhnw.dzr.drinks.data.CategoryDao;
import ch.fhnw.dzr.drinks.model.Category;
import ch.fhnw.dzr.drinks.model.DrinksStorageException;

public class CategoryDaoJpa implements CategoryDao {
    private EntityManager em;

    /**
     * Create a data access object (DAO) for category related database queries. This DAO uses JPA.
     * 
     * @param em the EntityManager to use with this DAO.
     */
    public CategoryDaoJpa(EntityManager em) {
        this.em = em;
    }

    // Queries
    /** Query to get all categories. */
    private static final String GET_ALL_CATEGORIES = "SELECT c FROM Category c WHERE c.name = c.type";
    /** Query to get all types of a category. */
    private static final String GET_ALL_TYPES_OF_CATEGORY = "SELECT c FROM Category c WHERE c.name LIKE :name";
    /** Delete category and all its types. */
    private static final String DELETE_CATEGORY = "DELETE Category c WHERE c.name = :name";

    @Override
    public List<Category> getAllCategories() {
        TypedQuery<Category> query = em.createQuery(GET_ALL_CATEGORIES, Category.class);
        List<Category> result = query.getResultList();
        return result;
    }

    @Override
    public List<Category> getAllTypesOf(String category) {
        TypedQuery<Category> query = em.createQuery(GET_ALL_TYPES_OF_CATEGORY, Category.class);
        query.setParameter("name", category);
        List<Category> result = query.getResultList();
        return result;
    }

    @Override
    public void saveOrUpdateCategory(Category cat) {
        executeInsideTransaction(em -> {
            if (cat.getId() != 0) {
                em.merge(cat);
            } else {
                em.persist(cat);
            }
        });
    }

    @Override
    public void deleteCategory(long id) {
        executeInsideTransaction(em -> {
            Category c = em.find(Category.class, id);
            if (c != null) {
                if (c.getName().equals(c.getType())) {
                    em.createQuery(DELETE_CATEGORY).setParameter("name", c.getName()).executeUpdate();
                } else {
                    em.remove(c);
                }
            }
        });
    }

    private void executeInsideTransaction(Consumer<EntityManager> action) {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            action.accept(em);
            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw new DrinksStorageException(e);
        }
    }

}
