package ch.fhnw.dzr.drinks.data;

import java.util.List;
import java.util.Optional;

import ch.fhnw.dzr.drinks.model.BarTool;

/**
 * Provides data for bar tools.
 */
public interface BarToolDao {

    /**
     * Retrieve all bar tools.
     * 
     * @param withPictures decides whether pictures (blobs) shall be included.
     * @return a list of all BarTool objects
     */
    List<BarTool> getAllBarTools(boolean withPictures);

    /**
     * Retrieves detail data about a given bar tool.
     * 
     * @param name the name of the bar tool.
     * @return The bar tool with the given name containing its description and picture (if available)
     */
    Optional<BarTool> getBarToolByName(String name);

    /**
     * Write BarTool object to database no matter if it is a new object or a corresponding entry
     * already exists.
     * 
     * @param u the BarTool to persist.
     */
    void saveOrUpdateBarTool(BarTool u);

    /**
     * Delete a bar tool with the given name.
     * 
     * @param name the name of the bar tool to delete.
     */
    void deleteBarTool(String name);

}
