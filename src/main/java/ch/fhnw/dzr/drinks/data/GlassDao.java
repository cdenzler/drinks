package ch.fhnw.dzr.drinks.data;

import java.util.List;
import java.util.Optional;

import ch.fhnw.dzr.drinks.model.Glass;

/**
 * Provides data for glasses.
 */
public interface GlassDao {

    /**
     * Retrieve all glasses.
     * 
     * @param withPictures decides whether pictures (blobs) shall be included.
     * @return a list of all Glass objects
     */
    List<Glass> getAllGlasses(boolean withPictures);

    /**
     * Retrieves detail data about a given glass.
     * 
     * @param name the name of the glass.
     * @return A glass object containing its description and picture.
     */
    Optional<Glass> getGlassByName(String name);

    /**
     * Write Glass object to database no matter if it is a new object or a corresponding entry
     * already exists.
     * 
     * @param g the glass to persist.
     */
    void saveOrUpdateGlass(Glass g);

    /**
     * Delete a glass with the given name.
     * 
     * @param name the name of the glass to delete.
     */
    void deleteGlass(String name);

}
