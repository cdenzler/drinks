package ch.fhnw.dzr.drinks.model;

public class DrinksStorageException extends RuntimeException {

    private static final long serialVersionUID = -1418432718207765197L;
    
    public DrinksStorageException(Exception cause) {
        super(cause);
    }

}
