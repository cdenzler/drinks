package ch.fhnw.dzr.drinks.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Represents a drink recipe. The main model class for the Drinks! application.
 */
@Entity
@Table(name = "DRINKS")
public class Drink extends Describable {

    @Column(name = "OCCASION", nullable = true)
    private String occasion;
    
    @Column(name = "INSTRUCTION", nullable = true)
    private String instruction;
    
    @Column(name = "NONALCOHOLIC")
    private boolean nonalcoholic;
    
    @ManyToOne
    @JoinColumn(name = "BARTOOLID", referencedColumnName = "ID")
    private BarTool barTool;
    
    @ManyToOne
    @JoinColumn(name = "GLASSID", referencedColumnName = "ID")
    private Glass glass;
    
    @OneToMany(mappedBy = "drink", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Ingredient> ingredients = new ArrayList<>();
    
    @ManyToMany
    @JoinTable(name = "DRKS_CATS",
        joinColumns = @JoinColumn(name = "DRK_ID", referencedColumnName = "ID"),
        inverseJoinColumns = @JoinColumn(name = "CAT_ID", referencedColumnName = "ID"))
    private Set<Category> categories = new HashSet<>();

    // Exception messages:
    /** When adding null as an ingredient. */
    public static final String MISSING_INGREDIENT = "Null instead of ingredient(s)";
    /** When trying to set a whole category instead of only a type thereof. */
    protected static final String NOT_A_VALID_CATEGORY = "Only single types of categories can be set.";

    /**
     * Used by Hibernate.
     */
    @SuppressWarnings("unused")
    private Drink() { }
    
    /**
     * A new Drink always needs a name.
     * 
     * @param name must not be null or empty.
     */
    public Drink(String name) {
        super(name);
    }

    /**
     * A good occasion/reason for enjoying this drink.
     * 
     * @return a good occasion.
     */
    public Optional<String> getOccasion() {
        return Optional.ofNullable(occasion);
    }

    /**
     * Give a good occasion/reason for enjoying this drink.
     * 
     * @param occasion a good occasion.
     */
    public void setOccasion(Optional<String> occasion) {
        this.occasion = occasion.isPresent() ? occasion.get() : null;
    }

    /**
     * Get instructions how to mix this drink.
     * 
     * @return instructions to mix this drink.
     */
    public Optional<String> getInstruction() {
        return Optional.ofNullable(instruction);
    }

    /**
     * Give instructions on how to mix this drink.
     * 
     * @param instruction instructions to mix this drink.
     */
    public void setInstruction(Optional<String> instruction) {
        this.instruction = instruction.isPresent() ? instruction.get() : null;
    }

    /**
     * Get all ingredients for this Drink.
     * 
     * @return an array of all ingredients of this drink.
     */
    public Ingredient[] getIngredients() {
        Ingredient[] result = new Ingredient[ingredients.size()];
        int i = 0;
        for (Ingredient ing: ingredients) {
            result[i] = ing;
            i++;
        }
        return result;
    }

    /**
     * Adds an ingredient to this recipe. If the name of the ingredient matches to one
     * of the existing ingredients, then this entry is updated with the parameters value, else a new
     * entry is added at the end of the list.
     * 
     * @param bottle the ingredient
     * @param quantity used for this drink
     * @param unit optional
     */
    public void addIngredient(Bottle bottle, double quantity, Optional<String> unit) {
        Ingredient ingredient = new Ingredient(this, bottle, quantity, unit);
        int pos = ingredients.indexOf(ingredient);
        if (pos < 0) {
            ingredients.add(ingredient);
        } else {
            ingredients.set(pos, ingredient);
        }
    }

    /**
     * Removes the specified ingredient. Only the name of the ingredient is important for finding it
     * in the ingredients list. Does nothing if ingredient is null or does not exist in the
     * ingredients list.
     * 
     * @param ingredient to remove.
     */
    public void removeIngredient(Bottle ingredient) {
        ingredients.remove(new Ingredient(null, ingredient, 0.0, Optional.empty()));
    }

    /**
     * Clears the list of ingredients.
     */
    public void clearIngredients() {
        ingredients.clear();
    }

    /**
     * Indicates whether this drink is non-alcoholic.
     * 
     * @return indicates whether this is a non-alcoholic drink.
     */
    public boolean isNonalcoholic() {
        return nonalcoholic;
    }

    /**
     * Indicates whether this drink is non-alcoholic.
     * 
     * @param nonalcoholic determine whether this drink is non-alcoholic.
     */
    public void setNonalcoholic(boolean nonalcoholic) {
        this.nonalcoholic = nonalcoholic;
    }

    /**
     * The bar tool used to mix this drink.
     * 
     * @return the bar tool.
     */
    public BarTool getBarTool() {
        return barTool;
    }

    /**
     * Determine bar tool used to mix this drink.
     * 
     * @param barTool to mix this drink.
     */
    public void setBarTool(BarTool barTool) {
        this.barTool = barTool;
    }

    /**
     * The glass that should be used to serve this drink.
     * 
     * @return the glass that should be used to serve this drink.
     */
    public Glass getGlass() {
        return glass;
    }

    /**
     * The glass that should be used to serve this drink.
     * 
     * @param glass the glass that should be used to serve this drink.
     */
    public void setGlass(Glass glass) {
        this.glass = glass;
    }

    /**
     * A drink category. Examples of such categories: Longdrink, Shortdrink, Collins etc.
     * 
     * @return the category.
     */
    public List<Category> getCategories() {
        return List.copyOf(categories);
    }

    /**
     * Adds a new category or replaces it if it is already a category of this drink.
     * Each category can only be added once.
     * @param category the type of a category.
     * @throws IllegalArgumentException if trying to set a category instead of a type.
     */
    public void addOrReplaceCategory(Category category) {
        if (category.getName().equals(category.getType())) {
            throw new IllegalArgumentException(NOT_A_VALID_CATEGORY);
        }
        categories.add(category);            
    }
    
    /**
     * Removes a category type.
     * @param category the category type to remove
     * @return whether the list of category changed due to the call of this method.
     */
    public boolean removeCategory(Category category) {
        return categories.remove(category);
    }
    
}
