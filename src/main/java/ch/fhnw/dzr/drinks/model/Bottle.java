package ch.fhnw.dzr.drinks.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * A bottle is an ingredient and has been purchased at a price.
 */
@Entity
@Table(name = "BOTTLES")
public class Bottle extends Describable {
    
    @Column(name = "PRICE", nullable = true)
    private Double price;
    
    @Column(name = "ABV", nullable = false)
    private Integer alcoholByVolume;
    
    @Column(name = "INSTOCK", nullable = false)
    private boolean inStock;
    
    @ManyToMany
    @JoinTable(name = "BOTS_CATS",
        joinColumns = @JoinColumn(name = "BOT_ID", referencedColumnName = "ID"),
        inverseJoinColumns = @JoinColumn(name = "CAT_ID", referencedColumnName = "ID"))
    private Set<Category> categories = new HashSet<>();

    // Exception messages:
    /** When trying to set a negative price. */
    protected static final String NEGATIVE_PRICE = "Price must not be negative.";
    /** When trying to set an illegal alcohol by volume value. */
    protected static final String WRONG_ALCOHOL_VOL = "Alcohol by volume must be between 0 and 100.";
    /** When trying to set a whole category instead of only a type thereof. */
    protected static final String NOT_A_VALID_CATEGORY = "Only single types of categories can be set.";

    /**
     * @param name must not be null or empty.
     * @param abv alcohol by volume.
     */
    public Bottle(String name, int abv) {
        super(name);
        setABV(abv);
    }

    /**
     * Used by Hibernate.
     */
    @SuppressWarnings("unused")
    private Bottle() {
    }

    /**
     * @return the price paid / to pay for the bottle.
     */
    public Optional<Double> getPrice() {
        return Optional.ofNullable(price);
    }

    /**
     * The price paid / to pay for the bottle.
     * 
     * @param price must be greater than 0.
     */
    public void setPrice(Optional<Double> price) {
        if (price.isPresent()) {
            if (price.get() < 0.0) {
                throw new IllegalArgumentException(NEGATIVE_PRICE);
            }
            this.price = price.get();
        } else {
            this.price = null;
        }
    }

    /**
     * The alcohol by volume contained in the bottle. If a value is present, then it
     * must be between 0 (non-alcoholic) and 100 (pure alcohol).
     * 
     * @return ABV.
     */
    public int getABV() {
        return alcoholByVolume;
    }

    /**
     * Alcohol by volume contained in the ingredient.
     * 
     * @param abv alcohol by volume. The value must be between 0 (non-alcoholic) and 100 (pure
     *            alcohol).
     * @throws IllegalArgumentException if abv is out of range [0, 100]
     */
    public void setABV(int abv) {
        if (abv < 0 || abv > 100) {
            throw new IllegalArgumentException(WRONG_ALCOHOL_VOL);
        }
        this.alcoholByVolume = abv;
    }

    /**
     * @return whether a bottle is in stock.
     */
    public boolean isInStock() {
        return inStock;
    }

    /**
     * @param inStock whether a bottle is in stock.
     */
    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }
    
    /**
     * Adds a new category type. Each category can only be added once.
     * @param category the type of a category.
     * @throws IllegalArgumentException if trying to set a category instead of a type.
     */
    public void addCategory(Category category) {
        if (category.getName().equals(category.getType())) {
            throw new IllegalArgumentException(NOT_A_VALID_CATEGORY);
        }
        categories.add(category);            
    }
    
    /**
     * Removes a category type.
     * @param category the category type to remove
     * @return whether the list of category changed due to the call of this method.
     */
    public boolean removeCategory(Category category) {
        return categories.remove(category);
    }
    
    /**
     * @return Retrieve all categories stored with this bottle.
     */
    public List<Category> getCategories() {
        return List.copyOf(categories);
    }

    @Override
    public boolean equals(Object o) {
        if (super.equals(o)) {
            Bottle bottle = (Bottle) o;
            return alcoholByVolume == bottle.alcoholByVolume;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), alcoholByVolume);
    }
}
