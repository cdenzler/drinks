package ch.fhnw.dzr.drinks.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A drink is served and/or mixed in a glass.
 */
@Entity
@Table(name = "GLASSES")
public class Glass extends Describable {

    @SuppressWarnings("unused")
    private Glass() {
    }

    /**
     * @param name must not be null or empty.
     */
    public Glass(String name) {
        super(name);
    }

}
