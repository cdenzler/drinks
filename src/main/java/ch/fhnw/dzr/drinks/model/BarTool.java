package ch.fhnw.dzr.drinks.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A drink is prepared using bar tools.
 */
@Entity
@Table(name = "BARTOOLS")
public class BarTool extends Describable {

    @SuppressWarnings("unused")
    private BarTool() {
    }

    /**
     * @param name must not be null or empty.
     */
    public BarTool(String name) {
        super(name);
    }

}
