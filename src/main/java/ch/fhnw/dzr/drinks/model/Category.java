package ch.fhnw.dzr.drinks.model;

import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A Category is basically a tag with a description. It can be applied to Ingredients and to Drinks.
 */
@Entity
@Table(name = "CATEGORIES")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", nullable = true)
    private String typeDescription;
    
    @Column(name = "TYPE", nullable = false)
    private String type;

    // Exception messages:
    /** When trying to set no or an empty string. */
    protected static final String NO_OR_EMPTY_STRING = "A non-empty String value is required.";

    @SuppressWarnings("unused")
    private  Category() {
    }

    /**
     * A Category must always be named.
     * @param name the new name for this Category.
     * @param type the type of this category.
     */
    public Category(String name, String type) {
        setName(name);
        setType(type);
    }
    
    /**
     * @return unique identification for this category.
     */
    public long getId() {
        return id;
    }

    /**
     * @return The name of a category object.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of this Category object. A name must always be a non-empty String.
     * 
     * @param name the new name to use for this Category.
     */
    public void setName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException(NO_OR_EMPTY_STRING);
        }
        this.name = name;
    }

    /**
     * @return describes the Category's type in a few sentences.
     */
    public Optional<String> getTypeDescription() {
        return Optional.ofNullable(typeDescription);
    }

    /**
     * @param description describes the Category's type in a few sentences.
     */
    public void setTypeDescription(Optional<String> description) {
        this.typeDescription = (description.isPresent()) ? description.get() : null;
    }
    
    /**
     * @return The type of a category object.
     */
    public String getType() {
        return type;
    }

    /**
     * Set the type of this Category object. A type must always be a non-empty String.
     * 
     * @param type the new type to use for this Category.
     */
    public void setType(String type) {
        if (type == null || type.isEmpty()) {
            throw new IllegalArgumentException(NO_OR_EMPTY_STRING);
        }
        this.type = type;
    }

}
