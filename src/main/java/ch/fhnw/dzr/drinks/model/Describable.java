package ch.fhnw.dzr.drinks.model;

import java.util.Objects;
import java.util.Optional;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;

/**
 * Objects that can be described and portrayed with a picture. Many classes in the Drinks! data
 * model have a name, an optional description and an optional picture. This is their base class.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Describable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private long id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Column(name = "DESCRIPTION", nullable = true)
    private String description;

    @Column(name = "PICTURE", nullable = true)
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] picture;

    // Exception messages:
    /** When trying to set no or an empty string. */
    protected static final String NO_OR_EMPTY_STRING = "A non-empty String value is required.";

    /**
     * Used by Hibernate
     */
    protected Describable() {
    }

    /**
     * A Describable must always be named.
     * @param name the new name for this Describable.
     */
    protected Describable(String name) {
        setName(name);
    }
    
    /**
     * @return the Describable's identifier
     */
    public long getId() {
        return id;
    }

    /**
     * @return The name of a describable object.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of this Describable object. A name must always be a non-empty String.
     * 
     * @param name the new name to use for this Describable.
     */
    public void setName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException(NO_OR_EMPTY_STRING);
        }
        this.name = name;
    }

    /**
     * @return describes the describable thing in a few sentences.
     */
    public Optional<String> getDescription() {
        return Optional.ofNullable(description);
    }

    /**
     * @param description describes the describable thing in a few sentences.
     */
    public void setDescription(Optional<String> description) {
        this.description = (description.isPresent()) ? description.get() : null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !getClass().isAssignableFrom(o.getClass())) {
            return false;
        }
        Describable that = (Describable) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    /**
     * Retrieve an image of this describable.
     * 
     * @return byte array containing image.
     */
    public Optional<byte[]> getPicture() {
        return Optional.ofNullable(picture);
    }

    /**
     * Stores the given byte array as an image for this describable.
     * 
     * @param picture if byte array is empty, Optional.empty() is stored.
     */
    public void setPicture(Optional<byte[]> picture) {
        if (picture != null && picture.isPresent()) {
            byte[] p = picture.get();
            this.picture = (p.length == 0) ? null : p;
        } else {
            this.picture = null;
        }
    }

}
