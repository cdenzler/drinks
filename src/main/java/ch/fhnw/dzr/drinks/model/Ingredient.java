package ch.fhnw.dzr.drinks.model;

import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * An ingredient is immutable type. It is part of the recipe and it is used to mix drinks.
 */
@Entity
@Table(name = "INGREDIENTS", 
       uniqueConstraints = { @UniqueConstraint(columnNames = { "BOTTLEID", "DRINKID" })
})
public class Ingredient {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private long id;

    @ManyToOne
    @JoinColumn(name = "BOTTLEID", referencedColumnName = "ID")
    private Bottle bottle;
    
    @Column(name = "QTY", nullable = false)
    private double quantity;
    
    @Column(name = "UNIT", nullable = true)
    private String unit;
    
    @ManyToOne
    @JoinColumn(name = "DRINKID", referencedColumnName = "ID")
    private Drink drink;
    
    // Exception messages:
    /** When trying to set a negative price. */
    protected static final String NEGATIVE_QUANTITY = "Quantity must not be negative.";
    /** When trying to set no or an empty string. */
    protected static final String NO_OR_EMPTY_STRING = "A non-empty String value is required.";
    /** When trying to pass null as unit parameter. */
    protected static final String NO_UNIT = "Unit must not be null";
    /** When trying to set no Drink. */
    protected static final String UNBOUND_INGREDIENT = "Ingredients cannot exist without Drinks, provide Drink";

    /**
     * @param bottle The Bottle that this ingredient represents.
     * @param quantity must not be negative.
     * @param unit the unit of that goes with the quantity.
     * @param drink must not be null.
     * @throws IllegalArgumentException if any of the parameters does not satisfy the preconditions.
     */
    public Ingredient(Drink drink, Bottle bottle, double quantity, Optional<String> unit) {
        setQuantity(quantity);
        setUnit(unit);
        setBottle(bottle);
        setDrink(drink);
    }
    
    Ingredient() { }
    
    void setBottle(Bottle bot) {
        this.bottle = bot;
    }
    
    /**
     * Convenience method to get the name of the bottle of this ingredient.
     * @return the ingredients name, never null or empty.
     */
    public String getName() {
        return bottle.getName();
    }
    
    /**
     * @return the ingredients content is from this bottle.
     */
    public Bottle getBottle() {
        return bottle;
    }
    
    /**
     * @return the ingredients quantity, never negative.
     */
    public double getQuantity() {
        return quantity;
    }
    
    void setQuantity(double quantity) {
        if (quantity < 0.0) {
            throw new IllegalArgumentException(NEGATIVE_QUANTITY);
        }
        this.quantity = quantity;
    }

    /**
     * @return the quantity's unit, never null.
     */
    public Optional<String> getUnit() {
        return Optional.ofNullable(unit);
    }
    
    void setUnit(Optional<String> unit) {
        this.unit = unit.isEmpty() ? null : unit.get();
    }
    
    /**
     * @return the Drink in which this ingredient is used.
     */
    public Drink getDrink() {
        return drink;
    }
    
    /**
     * @param drink the drink this ingredient is part of.
     */
    public void setDrink(Drink drink) {
        this.drink = drink;
    }
   
    /**
     * Compare ingredients only by their names.
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof Ingredient) {
            Ingredient ingr = (Ingredient) other;
            return this.bottle.getName().equals(ingr.bottle.getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return bottle.getName().hashCode();
    }

}
